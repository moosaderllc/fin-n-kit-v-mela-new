#ifndef _OBJECT
#define _OBJECT

#include <string>

#include "../Kuko/entities/BaseEntity.hpp"

enum ItemCategory { NONE, OBSTACLE, TRINKET, EXTRA_LIFE, LEVEL_END, UNSET };
enum Behavior { STILL = 0, JUMP_LEFT = 2, LEFT = 3, RANDOM = 4, UP = 5, DOWN = 1,
    SINE_LEFT = 6, SINE_UP = 7, SPIN = 8, CLOCKWISE = 9, UP_DOWN = 10, LEFT_RIGHT = 11 };

class Object : public kuko::BaseEntity
{
    public:
    Object();
    virtual ~Object();

    void Setup( const std::string& name, SDL_Texture* texture, kuko::FloatRect pos )
    {
        BaseEntity::Setup( name, texture, pos );
        SetType( name );
    }

    void SetType( const std::string& type );
    std::string GetType();
    std::string GetFriendlyName();
    ItemCategory GetCategory();
    void SetSelected( bool value );
    void SetBehavior( Behavior behavior );
    void SetBehavior( const std::string& behaviorName );
    void SetDimensions();
    Behavior GetBehavior();
    void UpdateFriendlyName();
    static std::string GetFriendlyName( const std::string& type );
    static std::string GetFriendlyBehavior( const std::string& behavior );
    static std::string GetFriendlyBehavior( Behavior behavior );
    static int GetBehaviorFrameX( Behavior behavior );
    float GetScale();
    void SetScale( float value );
    float GetSpeed();
    void SetSpeed( float value );
    void SetIsInEditor( bool value );
    bool GetIsInEditor();

    void Scroll( float amount );
    kuko::FloatRect GetDefaultPosition();
    void SetDefaultPosition( kuko::FloatRect pos );
    void SetBehaviorIcon();
    void SetPosition( float x, float y );
    void SetPosition( const kuko::FloatRect& pos );
    bool IsOffScreen();

    virtual void Update();
    virtual void Draw();

    static std::string GetBehaviorName( Behavior behavior );
    static Behavior GetBehaviorFromName( const std::string& name );

    private:
    std::string m_type;
    std::string m_friendlyName;
    float m_scale;
    ItemCategory m_category;
    Behavior m_behavior;
    float m_behaviorTimer;
    bool m_isSelected;
    kuko::FloatRect m_defaultPos;
    kuko::FloatRect m_defaultCollisionRegion;
    float m_speed;
    int m_randomDirection;
    bool m_editorState;
    kuko::Sprite m_behaviorIcon;
    bool m_onscreen;

    void BehaviorMovement();

    // ToDo: Extract into sub-classes
    void Behavior_Static();
    void Behavior_Left();
    void Behavior_Random();
    void Behavior_Up();
    void Behavior_JumpLeft();
    void Behavior_Down();
    void Behavior_SineLeft();
    void Behavior_SineUp();
    void Behavior_Spin();
    void Behavior_Clockwise();
    void Behavior_UpDown();
    void Behavior_LeftRight();
};

#endif
