#include "Object.hpp"

#include "../Kuko/base/Application.hpp"
#include "../Kuko/utilities/StringUtil.hpp"
#include "../Kuko/managers/ImageManager.hpp"

#include <cmath>

Object::Object()
{
    m_behaviorTimer = 0;
    m_behavior = STILL;
    m_friendlyName = "Bob";
    m_scale = 1;
    m_isSelected = false;
    m_category = UNSET;
    m_isSolid = true;
    m_editorState = false;
    m_onscreen = false;
}

Object::~Object()
{
}

void Object::Update()
{
    kuko::BaseEntity::Update();
    BehaviorMovement();
}

void Object::SetSelected( bool value )
{
    if ( m_isSelected == false && value == true )
    {
        m_defaultPos = m_position;
    }
    else if ( m_isSelected == true && value == false )
    {
        m_position = m_defaultPos;
    }

    m_isSelected = value;
}

void Object::SetType( const std::string& type )
{
    m_type = type;

    // Set obstacle type
    m_category = UNSET;
    if ( m_type.find( "obstacle" ) != std::string::npos )
    {
        m_category = OBSTACLE;
    }
    else if ( m_type.find( "trinket" ) != std::string::npos )
    {
        m_category = TRINKET;
    }
    else if ( m_type == "end_of_level" )
    {
        m_category = LEVEL_END;
    }
    else if ( m_type == "extra_heart" )
    {
        m_category = EXTRA_LIFE;
    }

    UpdateFriendlyName();
    SetDimensions();
}

void Object::UpdateFriendlyName()
{
    m_friendlyName = Object::GetFriendlyName( m_type );
}

std::string Object::GetFriendlyName( const std::string& type )
{
    if      ( type.find( "eel" ) != std::string::npos )           { return "Eel"; }
    else if ( type.find( "hawk" ) != std::string::npos )          { return "Hawk"; }
    else if ( type.find( "shark" ) != std::string::npos )         { return "Shark"; }
    else if ( type.find( "stingray" ) != std::string::npos )      { return "Stingray"; }
    else if ( type.find( "turtlecrab" ) != std::string::npos )    { return "Turtle"; }
    else if ( type.find( "mine" ) != std::string::npos )          { return "Mine"; }
    else if ( type.find( "balloon" ) != std::string::npos )        { return "Balloon"; }
    else if ( type.find( "bucket" ) != std::string::npos )         { return "Bucket"; }
    else if ( type.find( "starfish" ) != std::string::npos )       { return "Starfish"; }
    else if ( type.find( "fish" ) != std::string::npos )           { return "Fishie"; }
    else if ( type.find( "beachball" ) != std::string::npos )      { return "Beachball"; }
    else if ( type.find( "butterfly" ) != std::string::npos )      { return "Butterfly"; }
    else if ( type.find( "extra_heart" ) != std::string::npos )    { return "Heart"; }
    else if ( type.find( "end_of_level" ) != std::string::npos )   { return "Level End"; }
    return "";
}

void Object::SetDimensions()
{
    if ( m_type == "obstacle_mine" )
    {
        m_position.w = 120;
        m_position.h = 120;
        m_collisionRegion.x = 21;
        m_collisionRegion.y = 21;
        m_collisionRegion.w = 75;
        m_collisionRegion.h = 75;
    }
    else if ( m_type == "end_of_level" )
    {
        m_position.y = 0;
        m_position.h = 720;
        m_collisionRegion.h = 720;
    }
    else if ( m_type == "obstacle_eel" )
    {
        m_position.w = 80;
        m_position.h = 173;
        m_collisionRegion.x = 23;
        m_collisionRegion.y = 16;
        m_collisionRegion.w = 38;
        m_collisionRegion.h = 157;
    }
    else if ( m_type == "obstacle_shark" )
    {
        m_position.w = 227;
        m_position.h = 109;
        m_collisionRegion.x = 16;
        m_collisionRegion.y = 10;
        m_collisionRegion.w = 110;
        m_collisionRegion.h = 62;
    }
    else if ( m_type == "obstacle_turtlecrab" )
    {
        m_position.w = 166;
        m_position.h = 125;
        m_collisionRegion.x = 51;
        m_collisionRegion.y = 11;
        m_collisionRegion.w = 85;
        m_collisionRegion.h = 40;
    }
    else if ( m_type == "obstacle_stingray" )
    {
        m_position.w = 120;
        m_position.h = 70;
        m_collisionRegion.x = 3;
        m_collisionRegion.y = 7;
        m_collisionRegion.w = 53;
        m_collisionRegion.h = 44;
    }
    else if ( m_type == "obstacle_hawk" )
    {
        m_position.w = 104;
        m_position.h = 102;
        m_collisionRegion.x = 12;
        m_collisionRegion.y = 46;
        m_collisionRegion.w = 63;
        m_collisionRegion.h = 39;
    }
    else if ( m_type == "trinket_beachball" )
    {
        m_position.w = 85;
        m_position.h = 87;
        m_collisionRegion.x = 0;
        m_collisionRegion.y = 0;
        m_collisionRegion.w = 85;
        m_collisionRegion.h = 87;
    }
    else if ( m_type == "trinket_starfish" )
    {
        m_position.w = 80;
        m_position.h = 79;
        m_collisionRegion.x = 0;
        m_collisionRegion.y = 0;
        m_collisionRegion.w = 80;
        m_collisionRegion.h = 79;
    }
    else if ( m_type == "trinket_fish" )
    {
        m_position.w = 101;
        m_position.h = 70;
        m_collisionRegion.x = 0;
        m_collisionRegion.y = 0;
        m_collisionRegion.w = 101;
        m_collisionRegion.h = 70;
    }
    else if ( m_type == "trinket_bucket" )
    {
        m_position.w = 86;
        m_position.h = 112;
        m_collisionRegion.x = 0;
        m_collisionRegion.y = 0;
        m_collisionRegion.w = 86;
        m_collisionRegion.h = 112;
    }
    else if ( m_type == "trinket_butterfly" )
    {
        m_position.w = 45;
        m_position.h = 40;
        m_collisionRegion.x = 0;
        m_collisionRegion.y = 0;
        m_collisionRegion.w = 45;
        m_collisionRegion.h = 40;
    }
    else if ( m_type == "trinket_balloon" )
    {
        m_position.w = 64;
        m_position.h = 95;
        m_collisionRegion.x = 0;
        m_collisionRegion.y = 0;
        m_collisionRegion.w = 64;
        m_collisionRegion.h = 95;
    }

    m_defaultPos = m_position;
    m_defaultCollisionRegion = m_collisionRegion;
    kuko::BaseEntity::UpdateSprite();
}

std::string Object::GetType()
{
    return m_type;
}

std::string Object::GetFriendlyName()
{
    return m_friendlyName;
}

ItemCategory Object::GetCategory()
{
    return m_category;
}

Behavior Object::GetBehavior()
{
    return m_behavior;
}

void Object::SetBehavior( const std::string& behaviorName )
{
    Logger::Out( "Set behavior: " + behaviorName );
    Behavior b = GetBehaviorFromName( behaviorName );
    SetBehavior( b );
}

void Object::SetBehavior( Behavior behavior )
{
    m_behavior = behavior;
    SetBehaviorIcon();
}

float Object::GetScale()
{
    return m_scale;
}

void Object::SetScale( float value )
{
    m_scale = value;

    m_position.w = m_defaultPos.w * value;
    m_position.h = m_defaultPos.h * value;

    m_collisionRegion.w = m_defaultCollisionRegion.w * value;
    m_collisionRegion.h = m_defaultCollisionRegion.h * value;

    // Re-center it...
    m_position.x = m_defaultPos.x + ( m_defaultPos.w - m_position.w )/2;
    m_position.y = m_defaultPos.y + ( m_defaultPos.h - m_position.h )/2;

    // Change position relative to new x,y
    m_collisionRegion.x = m_defaultCollisionRegion.x * value;
    m_collisionRegion.y = m_defaultCollisionRegion.y * value;

    UpdateSprite();
}

void Object::SetSpeed( float value )
{
    m_speed = value;
}

float Object::GetSpeed()
{
    return m_speed;
}

std::string Object::GetBehaviorName( Behavior behavior )
{
    if ( behavior == STILL ) return "still";
    if ( behavior == JUMP_LEFT ) return "jump_left";
    if ( behavior == LEFT ) return "left";
    if ( behavior == RANDOM ) return "random";
    if ( behavior == UP ) return "up";
    if ( behavior == DOWN ) return "down";
    if ( behavior == SINE_LEFT ) return "sine_left";
    if ( behavior == SINE_UP ) return "sine_up";
    if ( behavior == SPIN ) return "spin";
    if ( behavior == CLOCKWISE ) return "clockwise";
    if ( behavior == UP_DOWN ) return "up_down";
    if ( behavior == LEFT_RIGHT ) return "left_right";
    return "";
}

Behavior Object::GetBehaviorFromName( const std::string& name )
{
    if ( name == "still" ) return STILL;
    if ( name == "jump_left" ) return JUMP_LEFT;
    if ( name == "left" ) return LEFT;
    if ( name == "random" ) return RANDOM;
    if ( name == "up" ) return UP;
    if ( name == "down" ) return DOWN;
    if ( name == "sine_left" ) return SINE_LEFT;
    if ( name == "sine_up" ) return SINE_UP;
    if ( name == "spin" ) return SPIN;
    if ( name == "clockwise" ) return CLOCKWISE;
    if ( name == "up_down" ) return UP_DOWN;
    if ( name == "left_right" ) return LEFT_RIGHT;
    return STILL;
}

std::string Object::GetFriendlyBehavior( const std::string& behavior )
{
    GetFriendlyBehavior( GetBehaviorFromName( behavior ) );
}

std::string Object::GetFriendlyBehavior( Behavior behavior )
{
    if ( behavior == STILL )                return "Still";
    if ( behavior == JUMP_LEFT )            return "Jump left";
    if ( behavior == LEFT )                 return "Left";
    if ( behavior == RANDOM )               return "Random";
    if ( behavior == UP )                   return "Up";
    if ( behavior == DOWN )                 return "Down";
    if ( behavior == SINE_LEFT )            return "Sinewave Left";
    if ( behavior == SINE_UP )              return "Sinewave Up";
    if ( behavior == SPIN )                 return "Spin";
    if ( behavior == CLOCKWISE )            return "Circle";
    if ( behavior == UP_DOWN )              return "Up-Down";
    if ( behavior == LEFT_RIGHT )           return "Left-Right";
    return "";
}

int Object::GetBehaviorFrameX( Behavior behavior )
{
    if ( behavior == STILL )                return 0;
    if ( behavior == JUMP_LEFT )            return 1;
    if ( behavior == LEFT )                 return 2;
    if ( behavior == RANDOM )               return 3;
    if ( behavior == UP )                   return 4;
    if ( behavior == DOWN )                 return 5;
    if ( behavior == SINE_LEFT )            return 6;
    if ( behavior == SINE_UP )              return 7;
    if ( behavior == SPIN )                 return 8;
    if ( behavior == CLOCKWISE )            return 9;
    if ( behavior == UP_DOWN )              return 10;
    if ( behavior == LEFT_RIGHT )           return 11;
    return 0;
}

void Object::Draw()
{
    kuko::BaseEntity::Draw();
    // Draw collision rect
//    kuko::FloatRect collision = m_collisionRegion;
//    collision.x += m_position.x;
//    collision.y += m_position.y;
    //kuko::ImageManager::DrawRectangle( collision, 0, 255, 0, 1 );

    if ( m_editorState )
    {
        m_behaviorIcon.Draw();
    }
}

void Object::BehaviorMovement()
{
    if ( IsOffScreen() )
    {
        // Don't do special behavior if off-screen,
        // just scroll them with the screen.
        m_position.x -= m_speed * kuko::Application::GetTimeStep();
    }
    else
    {
        m_onscreen = true;

        switch( m_behavior )
        {
            case LEFT:
                Behavior_Left();
            break;

            case RANDOM:
                Behavior_Random();
            break;

            case UP:
                Behavior_Up();
            break;

            case DOWN:
                Behavior_Down();
            break;

            case JUMP_LEFT:
                Behavior_JumpLeft();
            break;

            case SINE_LEFT:
                Behavior_SineLeft();
            break;

            case SINE_UP:
                Behavior_SineUp();
            break;

            case SPIN:
                Behavior_Spin();
            break;

            case CLOCKWISE:
                Behavior_Clockwise();
            break;

            case UP_DOWN:
                Behavior_UpDown();
            break;

            case LEFT_RIGHT:
                Behavior_LeftRight();
            break;

            default:
            Behavior_Static();
        }
    }
}

void Object::Behavior_Static()
{
    m_position.x -= m_speed * kuko::Application::GetTimeStep();
}

void Object::Behavior_Left()
{
    m_position.x -= m_speed * 2 * kuko::Application::GetTimeStep();
}

void Object::Behavior_Random()
{
    m_behaviorTimer += kuko::Application::GetTimeStep() * 500;
    if ( m_behaviorTimer >= 100 )
    {
        m_behaviorTimer = 0;
        // Choose new direction
        m_randomDirection = rand() % 4;
    }

    if ( m_randomDirection == 0 )
    {
        m_position.y -= m_speed * kuko::Application::GetTimeStep();
    }
    else if ( m_randomDirection == 1 )
    {
        m_position.x -= m_speed * kuko::Application::GetTimeStep();
    }
    else if ( m_randomDirection == 2 )
    {
        m_position.y += m_speed * kuko::Application::GetTimeStep();
    }
    else if ( m_randomDirection == 3 )
    {
        m_position.x += m_speed * kuko::Application::GetTimeStep();
    }

    m_position.x -= m_speed * kuko::Application::GetTimeStep();
}

void Object::Behavior_Up()
{
    m_position.x -= m_speed * kuko::Application::GetTimeStep();
    if ( m_position.x > kuko::Application::GetDefaultWidth() || m_position.x < -500 )
    {
        return;
    }
    m_position.y -= 0.3 * m_speed * kuko::Application::GetTimeStep();
}

void Object::Behavior_Down()
{
    if ( m_position.x > kuko::Application::GetDefaultWidth() || m_position.x < -500 )
    {
        m_position.x -= m_speed * kuko::Application::GetTimeStep();
    }
    else if ( m_position.y < kuko::Application::GetDefaultHeight() - 150 )
    {
        m_position.x -= m_speed * kuko::Application::GetTimeStep();
        m_position.y += 0.3 * m_speed * kuko::Application::GetTimeStep();
    }
    else
    {
        // At water level
        m_position.x -= 3 * m_speed * kuko::Application::GetTimeStep();
    }
}

void Object::Behavior_SineLeft()
{
    m_behaviorTimer += kuko::Application::GetTimeStep() * 500;
    if ( m_behaviorTimer >= 360 ) { m_behaviorTimer = 0; }

    float degrees = m_behaviorTimer;
    float rads = degrees * 3.14159 / 180;

    m_position.y = m_defaultPos.y + ( sin( rads ) * 50 );
    m_position.x -= m_speed * 2  * kuko::Application::GetTimeStep();
}

void Object::Behavior_SineUp()
{
    m_behaviorTimer += kuko::Application::GetTimeStep() * 500;
    if ( m_behaviorTimer >= 360 ) { m_behaviorTimer = 0; }

    float degrees = m_behaviorTimer;
    float rads = degrees * 3.14159 / 180;

    m_defaultPos.x -= m_speed * kuko::Application::GetTimeStep(); // Normal screen scrolling x
    m_position.x = m_defaultPos.x + ( sin ( rads ) * 50 ); // add sinewave
    m_position.y -= m_speed * kuko::Application::GetTimeStep(); // float up
}

void Object::Behavior_Spin()
{
    float rotation = GetRotation();
    rotation += 0.3;
    SetRotation( rotation );
    m_position.x -= m_speed * kuko::Application::GetTimeStep();
}

void Object::Behavior_Clockwise()
{
    m_behaviorTimer += kuko::Application::GetTimeStep() * 200;
    if ( m_behaviorTimer >= 360 ) { m_behaviorTimer = 0; }

    float degrees = m_behaviorTimer;
    float rads = degrees * 3.14159 / 180;
    float radius;

    m_position.x = m_defaultPos.x + ( cos( rads ) * 100 );
    m_position.y = m_defaultPos.y + ( sin( rads ) * 100 );
    m_defaultPos.x -= m_speed * kuko::Application::GetTimeStep();
}

void Object::Behavior_UpDown()
{
    m_behaviorTimer += 1;
    if ( m_behaviorTimer >= 1000 ) { m_behaviorTimer = 0; }

    if ( m_behaviorTimer < 500 )
    {
        m_position.y -= m_speed * kuko::Application::GetTimeStep();
    }
    else
    {
        m_position.y += m_speed * kuko::Application::GetTimeStep();
    }

    m_position.x -= m_speed * kuko::Application::GetTimeStep();
}

void Object::Behavior_LeftRight()
{
    m_behaviorTimer += 1;
    if ( m_behaviorTimer >= 1000 ) { m_behaviorTimer = 0; }

    if ( m_behaviorTimer < 250 )
    {
        m_position.x -= m_speed/2 * kuko::Application::GetTimeStep();
    }
    if ( m_behaviorTimer < 500 )
    {
        m_position.x += m_speed/2 * kuko::Application::GetTimeStep();
    }
    if ( m_behaviorTimer < 750 )
    {
        m_position.x += m_speed/2 * kuko::Application::GetTimeStep();
    }
    else
    {
        m_position.x -= m_speed/2 * kuko::Application::GetTimeStep();
    }

    m_position.x -= m_speed * kuko::Application::GetTimeStep();
}

void Object::Behavior_JumpLeft()
{
    m_behaviorTimer += kuko::Application::GetTimeStep() * 200;
    if ( m_behaviorTimer >= 720 ) { m_behaviorTimer = 0; }

    float degrees = m_behaviorTimer;
    float rads = degrees * 3.14159 / 180;

    m_position.x -= 1.5 * m_speed  * kuko::Application::GetTimeStep();
    m_position.y = m_defaultPos.y + ( sin( rads ) * 100 );
}

void Object::Scroll( float amount )
{
    // The scroll is actually being handled elseware.
}

kuko::FloatRect Object::GetDefaultPosition()
{
    return m_defaultPos;
}

void Object::SetDefaultPosition( kuko::FloatRect pos )
{
    m_defaultPos = pos;
}

void Object::SetIsInEditor( bool value )
{
    m_editorState = value;
    SetBehaviorIcon();
}

bool Object::GetIsInEditor()
{
    return m_editorState;
}

void Object::SetBehaviorIcon()
{
    if ( !m_editorState )
    {
        return;
    }

    m_behaviorIcon.SetTexture( kuko::ImageManager::GetTexture( "behavior_icons" ) );

    kuko::FloatRect position( m_position.x + m_position.w, m_position.y + m_position.h, 16, 16 );
    m_behaviorIcon.SetPosition( position );

    kuko::IntRect frame( GetBehaviorFrameX( m_behavior ) * position.w, 0, position.w, position.y );
    m_behaviorIcon.SetFrame( frame );
}

void Object::SetPosition( float x, float y )
{
    kuko::BaseEntity::SetPosition( x, y );
    SetBehaviorIcon();
}

void Object::SetPosition( const kuko::FloatRect& pos )
{
    kuko::BaseEntity::SetPosition( pos );
    SetBehaviorIcon();
}

bool Object::IsOffScreen()
{
    bool xOffscreen = ( m_position.x > kuko::Application::GetDefaultWidth() || m_position.x + m_position.w < 0 );
    if ( m_onscreen ) { return false; }
    return xOffscreen;
}
