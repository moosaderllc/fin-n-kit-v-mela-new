#ifndef JUMPING_OBJECT_HPP
#define JUMPING_OBJECT_HPP

#include <string>

#include "../Kuko/entities/BaseEntity.hpp"

class JumpingObject : public kuko::BaseEntity
{
    public:
    JumpingObject();

    void Reset();

    virtual void Update();
    virtual void Update( float timeStep );

    void SetPhysics( float gravity, float initVelocity, bool canDoubleJump, float maxY );

    virtual void Draw();
    bool IsJumping();
    void BeginJump();
    void Fall( float timeStep );
    void HitGround();
    void PushUp( const kuko::BaseEntity& pusher );
    void Launch();
    void SetName( const std::string& name );

    void SetDoubleJumpAbility( bool value );

    void SetJumpFrame( const kuko::IntRect& rect );
    void SetDefaultFrame( const kuko::IntRect& rect );

    private:
    float m_jumpCounter;
    float m_velocityY;
    float m_gravity;
    float m_launchCounter;
    float m_initialVelocity;
    bool m_canDoubleJump;
    int m_jumpCount;
    float m_maxY;
    std::string m_name;

    kuko::IntRect m_defaultFrame;
    kuko::IntRect m_jumpFrame;
};

#endif
