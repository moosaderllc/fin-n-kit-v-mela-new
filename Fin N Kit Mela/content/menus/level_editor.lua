assets = {
    { name = "background",          path = "content/graphics/ui/editor/editor_background.png" },
    { name = "editor_maximize",     path = "content/graphics/ui/editor/editor_maximize.png" },
    { name = "editor_minimize",     path = "content/graphics/ui/editor/editor_minimize.png" },
    { name = "editor_tabs",         path = "content/graphics/ui/editor/editor_tabs.png" },
    { name = "grid",                path = "content/graphics/ui/editor/grid.png" },
    { name = "behavior_buttons",    path = "content/graphics/ui/editor/behavior_buttons.png" },
    { name = "behavior_icons",      path = "content/graphics/ui/editor/behavior_tiny.png" },
    { name = "btn_music",           path = "content/graphics/ui/editor/btn_music.png" },
    { name = "btn_blank",           path = "content/graphics/ui/editor/btn_blank.png" },
    { name = "editor_file_buttons", path = "content/graphics/ui/editor/editor_file_buttons.png" },
    { name = "scale_buttons",       path = "content/graphics/ui/editor/scale_buttons.png" },
    { name = "button_exit",         path = "content/graphics/ui/btn_exit_level.png" },
    { name = "editor_btn_left",     path = "content/graphics/ui/editor/editor_btn_left.png" },
    { name = "editor_btn_right",    path = "content/graphics/ui/editor/editor_btn_right.png" },

    { name = "trinket_balloon",     path = "content/graphics/game/trinket_balloon.png" },
    { name = "trinket_bucket",      path = "content/graphics/game/trinket_bucket.png" },
    { name = "trinket_fish",        path = "content/graphics/game/trinket_fish.png" },
    { name = "trinket_starfish",    path = "content/graphics/game/trinket_starfish.png" },
    { name = "trinket_beachball",   path = "content/graphics/game/trinket_beachball.png" },
    { name = "trinket_butterfly",   path = "content/graphics/game/trinket_butterfly.png" },

    { name = "obstacle_eel",        path = "content/graphics/game/obstacle_eel.png" },
    { name = "obstacle_hawk",       path = "content/graphics/game/obstacle_hawk.png" },
    { name = "obstacle_shark",      path = "content/graphics/game/obstacle_shark.png" },
    { name = "obstacle_stingray",   path = "content/graphics/game/obstacle_stingray.png" },
    { name = "obstacle_turtlecrab", path = "content/graphics/game/obstacle_turtlecrab.png" },
    { name = "obstacle_mine",       path = "content/graphics/game/obstacle_mine.png" },

    { name = "extra_heart",         path = "content/graphics/game/extra_heart.png" },
    { name = "end_of_level",        path = "content/graphics/game/end_of_level_tall.png" },

    { name = "background_sand",     path = "content/graphics/game/background_sand.png" },
    { name = "background_tank",     path = "content/graphics/game/background_tank.png" },
    { name = "background_night",    path = "content/graphics/game/background_night.png" },

    { name = "background_waves",        path = "content/graphics/game/background_waves.png" },
    { name = "foreground_waves",        path = "content/graphics/game/foreground_waves.png" },
    { name = "background_waves2",        path = "content/graphics/game/background_waves2.png" },
    { name = "foreground_waves2",        path = "content/graphics/game/foreground_waves2.png" },
    { name = "background_waves3",        path = "content/graphics/game/background_waves3.png" },
    { name = "foreground_waves3",        path = "content/graphics/game/foreground_waves3.png" },
}

menu_options = {
    total_pages = 5
}

scrw = 1280
scrh = 720

toolbarWH = 80
toolbarX = scrw-toolbarWH
rightColX = toolbarX-280
rightColY = 10
spacingY = 50

headerHeight = 40
subHeaderHeight = 30

textr = 0
textg = 0
textb = 0

screenWidth = 1280
screenHeight = 720

toolbarWidth = 260
toolbarHeight = 652
toolbarX = screenWidth - toolbarWidth
toolbarY = screenHeight - toolbarHeight

tabWidth = 65
tabHeight = 65

thirdButtonWidth = 75
objectWidth = 50
objectSpacingX = 75

headerHeight = 25

textr = 255
textg = 255
textb = 255

elements = {
    -- Common
    { ui_type = "button", id = "button_exit",   texture_id = "button_exit",         x = 10, y = 10, width = 60, height = 60 },
    { ui_type = "button", id = "btn_left",      texture_id = "editor_btn_left",     x = 0, y = screenHeight - 80, width = 80, height = 80 },
    { ui_type = "button", id = "btn_right",     texture_id = "editor_btn_right",    x = 80, y = screenHeight - 80, width = 80, height = 80 },

    -- Page 1: Objects
    { page = 1, ui_type = "image", id = "bg1", texture_id = "background", x = toolbarX, y = toolbarY, width = toolbarWidth, height = toolbarHeight },
    { page = 1, ui_type = "button", id = "minimize_editor_1", texture_id = "editor_minimize", x = 1000, y = 322, width = 23, height = 62 },

    { page = 1, ui_type = "button", id = "page1_goto1", texture_id = "editor_tabs", x = toolbarX+tabWidth*0, y = 0, width = tabWidth, height = tabHeight, frame_x = tabWidth*0, frame_y = 0,            frame_width = tabWidth, frame_height = tabHeight },
    { page = 1, ui_type = "button", id = "page1_goto2", texture_id = "editor_tabs", x = toolbarX+tabWidth*1, y = 0, width = tabWidth, height = tabHeight, frame_x = tabWidth*1, frame_y = tabHeight,    frame_width = tabWidth, frame_height = tabHeight },
    { page = 1, ui_type = "button", id = "page1_goto3", texture_id = "editor_tabs", x = toolbarX+tabWidth*2, y = 0, width = tabWidth, height = tabHeight, frame_x = tabWidth*2, frame_y = tabHeight,    frame_width = tabWidth, frame_height = tabHeight },
    { page = 1, ui_type = "button", id = "page1_goto4", texture_id = "editor_tabs", x = toolbarX+tabWidth*3, y = 0, width = tabWidth, height = tabHeight, frame_x = tabWidth*3, frame_y = tabHeight,    frame_width = tabWidth, frame_height = tabHeight },

    -- labels
    { page = 1, ui_type = "label",  id = "editor_obstacles",  text_id = "editor_obstacles",   x = toolbarX + 10, y = toolbarY + 10 + 000, width = 0, height = headerHeight, font_r = textr, font_g = textg, font_b = textb, font_a = 255, font_id = "main", shadow_r = 0, shadow_g = 88, shadow_b = 227, shadow_a = 255, shadow_offset_x = 2, shadow_offset_y = 2, use_shadow = 1 },
    { page = 1, ui_type = "label",  id = "editor_trinkets",   text_id = "editor_trinkets",    x = toolbarX + 10, y = toolbarY + 10 + 150, width = 0, height = headerHeight, font_r = textr, font_g = textg, font_b = textb, font_a = 255, font_id = "main", shadow_r = 0, shadow_g = 88, shadow_b = 227, shadow_a = 255, shadow_offset_x = 2, shadow_offset_y = 2, use_shadow = 1 },
    { page = 1, ui_type = "label",  id = "editor_extra_life", text_id = "editor_extra_life",  x = toolbarX + 10, y = toolbarY + 10 + 300, width = 0, height = headerHeight, font_r = textr, font_g = textg, font_b = textb, font_a = 255, font_id = "main", shadow_r = 0, shadow_g = 88, shadow_b = 227, shadow_a = 255, shadow_offset_x = 2, shadow_offset_y = 2, use_shadow = 1 },
    { page = 1, ui_type = "label",  id = "editor_level_end",  text_id = "editor_level_end",   x = toolbarX + 10, y = toolbarY + 10 + 375, width = 0, height = headerHeight, font_r = textr, font_g = textg, font_b = textb, font_a = 255, font_id = "main", shadow_r = 0, shadow_g = 88, shadow_b = 227, shadow_a = 255, shadow_offset_x = 2, shadow_offset_y = 2, use_shadow = 1 },

    -- obstacles
    { page = 1, ui_type = "button", id = "btn_obstacle_eel",        texture_id = "obstacle_eel",        x = toolbarX+35+objectSpacingX*0,  y = toolbarY+40,             width = objectWidth, height = objectWidth },
    { page = 1, ui_type = "button", id = "btn_obstacle_hawk",       texture_id = "obstacle_hawk",       x = toolbarX+35+objectSpacingX*1,  y = toolbarY+40,             width = objectWidth, height = objectWidth },
    { page = 1, ui_type = "button", id = "btn_obstacle_shark",      texture_id = "obstacle_shark",      x = toolbarX+35+objectSpacingX*2,  y = toolbarY+40,             width = objectWidth, height = objectWidth },
    { page = 1, ui_type = "button", id = "btn_obstacle_stingray",   texture_id = "obstacle_stingray",   x = toolbarX+35+objectSpacingX*0,  y = toolbarY+40+objectWidth, width = objectWidth, height = objectWidth },
    { page = 1, ui_type = "button", id = "btn_obstacle_turtlecrab", texture_id = "obstacle_turtlecrab", x = toolbarX+35+objectSpacingX*1,  y = toolbarY+40+objectWidth, width = objectWidth, height = objectWidth },
    { page = 1, ui_type = "button", id = "btn_obstacle_mine",       texture_id = "obstacle_mine",       x = toolbarX+35+objectSpacingX*2,  y = toolbarY+40+objectWidth, width = objectWidth, height = objectWidth },
    -- trinkets
    { page = 1, ui_type = "button", id = "btn_trinket_balloon",     texture_id = "trinket_balloon",     x = toolbarX+35+objectSpacingX*0,  y = toolbarY+190,             width = objectWidth, height = objectWidth },
    { page = 1, ui_type = "button", id = "btn_trinket_bucket",      texture_id = "trinket_bucket",      x = toolbarX+35+objectSpacingX*1,  y = toolbarY+190,             width = objectWidth, height = objectWidth },
    { page = 1, ui_type = "button", id = "btn_trinket_fish",        texture_id = "trinket_fish",        x = toolbarX+35+objectSpacingX*2,  y = toolbarY+190,             width = objectWidth, height = objectWidth },
    { page = 1, ui_type = "button", id = "btn_trinket_starfish",    texture_id = "trinket_starfish",    x = toolbarX+35+objectSpacingX*0,  y = toolbarY+190+objectWidth, width = objectWidth, height = objectWidth },
    { page = 1, ui_type = "button", id = "btn_trinket_beachball",   texture_id = "trinket_beachball",   x = toolbarX+35+objectSpacingX*1,  y = toolbarY+190+objectWidth, width = objectWidth, height = objectWidth },
    { page = 1, ui_type = "button", id = "btn_trinket_butterfly",   texture_id = "trinket_butterfly",   x = toolbarX+35+objectSpacingX*2,  y = toolbarY+190+objectWidth, width = objectWidth, height = objectWidth },
    -- extra life
    { page = 1, ui_type = "button", id = "btn_extra_heart",         texture_id = "extra_heart",         x = toolbarX+35+objectSpacingX*1,  y = toolbarY+330,             width = objectWidth, height = objectWidth },
    -- end of level
    { page = 1, ui_type = "button", id = "btn_end_of_level",        texture_id = "end_of_level",        x = toolbarX+35+objectSpacingX*1,  y = toolbarY+410,             width = objectWidth, height = objectWidth },

    -- Page 2: Object Properties
    { page = 2, ui_type = "image", id = "bg2", texture_id = "background", x = toolbarX, y = toolbarY, width = toolbarWidth, height = toolbarHeight },
    { page = 2, ui_type = "button", id = "minimize_editor_2", texture_id = "editor_minimize", x = 1000, y = 322, width = 23, height = 62 },

    { page = 2, ui_type = "button", id = "page2_goto1", texture_id = "editor_tabs", x = toolbarX+tabWidth*0, y = 0, width = tabWidth, height = tabHeight, frame_x = tabWidth*0, frame_y = tabHeight,    frame_width = tabWidth, frame_height = tabHeight },
    { page = 2, ui_type = "button", id = "page2_goto2", texture_id = "editor_tabs", x = toolbarX+tabWidth*1, y = 0, width = tabWidth, height = tabHeight, frame_x = tabWidth*1, frame_y = 0,            frame_width = tabWidth, frame_height = tabHeight },
    { page = 2, ui_type = "button", id = "page2_goto3", texture_id = "editor_tabs", x = toolbarX+tabWidth*2, y = 0, width = tabWidth, height = tabHeight, frame_x = tabWidth*2, frame_y = tabHeight,    frame_width = tabWidth, frame_height = tabHeight },
    { page = 2, ui_type = "button", id = "page2_goto4", texture_id = "editor_tabs", x = toolbarX+tabWidth*3, y = 0, width = tabWidth, height = tabHeight, frame_x = tabWidth*3, frame_y = tabHeight,    frame_width = tabWidth, frame_height = tabHeight },

    -- Labels
    { page = 2, ui_type = "label",  id = "editor_behavior",   text_id = "editor_behavior",  x = toolbarX + 10, y = toolbarY + 10 + 000, width = 0, height = headerHeight, font_r = textr, font_g = textg, font_b = textb, font_a = 255, font_id = "main", shadow_r = 0, shadow_g = 88, shadow_b = 227, shadow_a = 255, shadow_offset_x = 2, shadow_offset_y = 2, use_shadow = 1 },
    { page = 2, ui_type = "label",  id = "editor_scale",      text_id = "editor_scale",     x = toolbarX + 10, y = toolbarY + 10 + 200, width = 0, height = headerHeight, font_r = textr, font_g = textg, font_b = textb, font_a = 255, font_id = "main", shadow_r = 0, shadow_g = 88, shadow_b = 227, shadow_a = 255, shadow_offset_x = 2, shadow_offset_y = 2, use_shadow = 1 },

    -- Behavior buttons
    { page = 2, ui_type = "button", id = "behavior_still",              texture_id = "behavior_buttons", x = toolbarX + 10,  y = toolbarY + 50, width = 40, height = 40, frame_x = 0,   frame_y = 0, frame_width = 40, frame_height = 40 },
    { page = 2, ui_type = "button", id = "behavior_down",               texture_id = "behavior_buttons", x = toolbarX + 60,  y = toolbarY + 50, width = 40, height = 40, frame_x = 200, frame_y = 0, frame_width = 40, frame_height = 40 },

    { page = 2, ui_type = "button", id = "behavior_jump",               texture_id = "behavior_buttons", x = toolbarX + 10,  y = toolbarY + 100, width = 40, height = 40, frame_x = 40,  frame_y = 0, frame_width = 40, frame_height = 40 },
    { page = 2, ui_type = "button", id = "behavior_left",               texture_id = "behavior_buttons", x = toolbarX + 110, y = toolbarY + 50, width = 40, height = 40, frame_x = 80,  frame_y = 0, frame_width = 40, frame_height = 40 },
    { page = 2, ui_type = "button", id = "behavior_right",              texture_id = "behavior_buttons", x = toolbarX + 160, y = toolbarY + 50, width = 40, height = 40, frame_x = 120, frame_y = 0, frame_width = 40, frame_height = 40 },
    { page = 2, ui_type = "button", id = "behavior_up",                 texture_id = "behavior_buttons", x = toolbarX + 210, y = toolbarY + 50, width = 40, height = 40, frame_x = 160, frame_y = 0, frame_width = 40, frame_height = 40 },

    { page = 2, ui_type = "button", id = "behavior_sine_left",          texture_id = "behavior_buttons", x = toolbarX + 60,  y = toolbarY + 100, width = 40, height = 40, frame_x = 240, frame_y = 0, frame_width = 40, frame_height = 40 },
    { page = 2, ui_type = "button", id = "behavior_sine_right",         texture_id = "behavior_buttons", x = toolbarX + 110, y = toolbarY + 100, width = 40, height = 40, frame_x = 280, frame_y = 0, frame_width = 40, frame_height = 40 },
    { page = 2, ui_type = "button", id = "behavior_counter_clockwise",  texture_id = "behavior_buttons", x = toolbarX + 160, y = toolbarY + 100, width = 40, height = 40, frame_x = 320, frame_y = 0, frame_width = 40, frame_height = 40 },
    { page = 2, ui_type = "button", id = "behavior_clockwise",          texture_id = "behavior_buttons", x = toolbarX + 210, y = toolbarY + 100, width = 40, height = 40, frame_x = 360, frame_y = 0, frame_width = 40, frame_height = 40 },
    { page = 2, ui_type = "button", id = "behavior_updown",             texture_id = "behavior_buttons", x = toolbarX + 10,  y = toolbarY + 150, width = 40, height = 40, frame_x = 400, frame_y = 0, frame_width = 40, frame_height = 40 },
    { page = 2, ui_type = "button", id = "behavior_leftright",          texture_id = "behavior_buttons", x = toolbarX + 60,  y = toolbarY + 150, width = 40, height = 40, frame_x = 440, frame_y = 0, frame_width = 40, frame_height = 40 },

    { page = 2, ui_type = "button", id = "scale_50",                    texture_id = "scale_buttons",   x = toolbarX + 10,   y = toolbarY + 240, width = 40, height = 40, frame_x = 0,   frame_y = 0, frame_width = 40, frame_height = 40 },
    { page = 2, ui_type = "button", id = "scale_100",                   texture_id = "scale_buttons",   x = toolbarX + 10,   y = toolbarY + 290, width = 40, height = 40, frame_x = 40,  frame_y = 0, frame_width = 40, frame_height = 40 },
    { page = 2, ui_type = "button", id = "scale_150",                   texture_id = "scale_buttons",   x = toolbarX + 10,   y = toolbarY + 340, width = 40, height = 40, frame_x = 80,  frame_y = 0, frame_width = 40, frame_height = 40 },

    { page = 2, ui_type = "label",  id = "lbl_scale_50",                text_id = "editor_scale_50",    x = toolbarX+75,    y = toolbarY+240, width = 0, height = headerHeight, font_r = textr, font_g = textg, font_b = textb, font_a = 255, font_id = "main", shadow_r = 0, shadow_g = 88, shadow_b = 227, shadow_a = 255, shadow_offset_x = 2, shadow_offset_y = 2, use_shadow = 1 },
    { page = 2, ui_type = "label",  id = "lbl_scale_100",               text_id = "editor_scale_100",   x = toolbarX+75,    y = toolbarY+290, width = 0, height = headerHeight, font_r = textr, font_g = textg, font_b = textb, font_a = 255, font_id = "main", shadow_r = 0, shadow_g = 88, shadow_b = 227, shadow_a = 255, shadow_offset_x = 2, shadow_offset_y = 2, use_shadow = 1 },
    { page = 2, ui_type = "label",  id = "lbl_scale_150",               text_id = "editor_scale_150",   x = toolbarX+75,    y = toolbarY+340, width = 0, height = headerHeight, font_r = textr, font_g = textg, font_b = textb, font_a = 255, font_id = "main", shadow_r = 0, shadow_g = 88, shadow_b = 227, shadow_a = 255, shadow_offset_x = 2, shadow_offset_y = 2, use_shadow = 1 },


    -- Page 3: Level Properties
    { page = 3, ui_type = "image",  id = "bg3", texture_id = "background", x = toolbarX, y = toolbarY, width = toolbarWidth, height = toolbarHeight },
    { page = 3, ui_type = "button", id = "minimize_editor_3", texture_id = "editor_minimize", x = 1000, y = 322, width = 23, height = 62 },

    { page = 3, ui_type = "button", id = "page3_goto1", texture_id = "editor_tabs", x = toolbarX+tabWidth*0, y = 0, width = tabWidth, height = tabHeight, frame_x = tabWidth*0, frame_y = tabHeight,    frame_width = tabWidth, frame_height = tabHeight },
    { page = 3, ui_type = "button", id = "page3_goto2", texture_id = "editor_tabs", x = toolbarX+tabWidth*1, y = 0, width = tabWidth, height = tabHeight, frame_x = tabWidth*1, frame_y = tabHeight,    frame_width = tabWidth, frame_height = tabHeight },
    { page = 3, ui_type = "button", id = "page3_goto3", texture_id = "editor_tabs", x = toolbarX+tabWidth*2, y = 0, width = tabWidth, height = tabHeight, frame_x = tabWidth*2, frame_y = 0,            frame_width = tabWidth, frame_height = tabHeight },
    { page = 3, ui_type = "button", id = "page3_goto4", texture_id = "editor_tabs", x = toolbarX+tabWidth*3, y = 0, width = tabWidth, height = tabHeight, frame_x = tabWidth*3, frame_y = tabHeight,    frame_width = tabWidth, frame_height = tabHeight },

    -- Labels
    { page = 3, ui_type = "label",  id = "editor_background",   text_id = "editor_background",  x = toolbarX + 10, y = toolbarY + 10 + 000, width = 0, height = headerHeight, font_r = textr, font_g = textg, font_b = textb, font_a = 255, font_id = "main", shadow_r = 0, shadow_g = 88, shadow_b = 227, shadow_a = 255, shadow_offset_x = 2, shadow_offset_y = 2, use_shadow = 1 },
    { page = 3, ui_type = "label",  id = "editor_ground",       text_id = "editor_ground",      x = toolbarX + 10, y = toolbarY + 10 + 150, width = 0, height = headerHeight, font_r = textr, font_g = textg, font_b = textb, font_a = 255, font_id = "main", shadow_r = 0, shadow_g = 88, shadow_b = 227, shadow_a = 255, shadow_offset_x = 2, shadow_offset_y = 2, use_shadow = 1 },
    { page = 3, ui_type = "label",  id = "editor_music",        text_id = "editor_music",       x = toolbarX + 10, y = toolbarY + 10 + 270, width = 0, height = headerHeight, font_r = textr, font_g = textg, font_b = textb, font_a = 255, font_id = "main", shadow_r = 0, shadow_g = 88, shadow_b = 227, shadow_a = 255, shadow_offset_x = 2, shadow_offset_y = 2, use_shadow = 1 },
    { page = 3, ui_type = "label",  id = "editor_scroll",       text_id = "editor_scroll",      x = toolbarX + 10, y = toolbarY + 10 + 370, width = 0, height = headerHeight, font_r = textr, font_g = textg, font_b = textb, font_a = 255, font_id = "main", shadow_r = 0, shadow_g = 88, shadow_b = 227, shadow_a = 255, shadow_offset_x = 2, shadow_offset_y = 2, use_shadow = 1 },

    -- Backgrounds
    { page = 3, ui_type = "button", id = "background1", texture_id = "background_sand",     x = toolbarX+35+objectSpacingX*0,  y = toolbarY+60,   width = objectWidth, height = objectWidth },
    { page = 3, ui_type = "button", id = "background2", texture_id = "background_tank",     x = toolbarX+35+objectSpacingX*1,  y = toolbarY+60,   width = objectWidth, height = objectWidth },
    { page = 3, ui_type = "button", id = "background3",texture_id = "background_night",     x = toolbarX+35+objectSpacingX*2,  y = toolbarY+60,  width = objectWidth, height = objectWidth },

    -- Grounds
    { page = 3, ui_type = "button", id = "ground1",    texture_id = "background_waves",   x = toolbarX+35+objectSpacingX*0,  y = toolbarY+200,   width = objectWidth, height = objectWidth },
    { page = 3, ui_type = "button", id = "ground2",   texture_id = "background_waves2",   x = toolbarX+35+objectSpacingX*1,  y = toolbarY+200,   width = objectWidth, height = objectWidth },
    { page = 3, ui_type = "button", id = "ground3",   texture_id = "background_waves3",   x = toolbarX+35+objectSpacingX*2,  y = toolbarY+200,   width = objectWidth, height = objectWidth },

    -- Songs
    { page = 3, ui_type = "button", id = "song1", texture_id = "btn_music", x = toolbarX+10,  y = toolbarY+320, width = 40, height = 40 },
    { page = 3, ui_type = "button", id = "song2", texture_id = "btn_music", x = toolbarX+60,  y = toolbarY+320, width = 40, height = 40 },
    { page = 3, ui_type = "button", id = "song3", texture_id = "btn_music", x = toolbarX+110,  y = toolbarY+320, width = 40, height = 40 },

--    { page = 3, ui_type = "label",  id = "editor_song_1", text_id = "editor_song_1", x = toolbarX+75, y = toolbarY+320, width = 0, height = headerHeight, font_r = textr, font_g = textg, font_b = textb, font_a = 255, font_id = "main", shadow_r = 0, shadow_g = 88, shadow_b = 227, shadow_a = 255, shadow_offset_x = 2, shadow_offset_y = 2, use_shadow = 1 },
--    { page = 3, ui_type = "label",  id = "editor_song_2", text_id = "editor_song_2", x = toolbarX+75, y = toolbarY+370, width = 0, height = headerHeight, font_r = textr, font_g = textg, font_b = textb, font_a = 255, font_id = "main", shadow_r = 0, shadow_g = 88, shadow_b = 227, shadow_a = 255, shadow_offset_x = 2, shadow_offset_y = 2, use_shadow = 1 },
--    { page = 3, ui_type = "label",  id = "editor_song_3", text_id = "editor_song_3", x = toolbarX+75, y = toolbarY+420, width = 0, height = headerHeight, font_r = textr, font_g = textg, font_b = textb, font_a = 255, font_id = "main", shadow_r = 0, shadow_g = 88, shadow_b = 227, shadow_a = 255, shadow_offset_x = 2, shadow_offset_y = 2, use_shadow = 1 },

    -- Speeds
    { page = 3, ui_type = "button", id = "btn_speed1", texture_id = "btn_blank", x = toolbarX+10,  y = toolbarY+420, width = 40, height = 40, frame_x = 0, frame_y = 0, frame_width = 40, frame_height = 40 },
    { page = 3, ui_type = "button", id = "btn_speed2", texture_id = "btn_blank", x = toolbarX+60,  y = toolbarY+420, width = 40, height = 40, frame_x = 0, frame_y = 0, frame_width = 40, frame_height = 40 },
    { page = 3, ui_type = "button", id = "btn_speed3", texture_id = "btn_blank", x = toolbarX+110,  y = toolbarY+420, width = 40, height = 40, frame_x = 0, frame_y = 0, frame_width = 40, frame_height = 40 },
    { page = 3, ui_type = "button", id = "btn_speed4", texture_id = "btn_blank", x = toolbarX+160,  y = toolbarY+420, width = 40, height = 40, frame_x = 0, frame_y = 0, frame_width = 40, frame_height = 40 },

    { page = 3, ui_type = "label",  id = "editor_speed_1", text = "1", x = toolbarX+25, y = toolbarY+425, width = 0, height = headerHeight, font_r = 0, font_g = 0, font_b = 0, font_a = 255, font_id = "main", shadow_r = 0, shadow_g = 88, shadow_b = 227, shadow_a = 255, shadow_offset_x = 2, shadow_offset_y = 2, use_shadow = 1 },
    { page = 3, ui_type = "label",  id = "editor_speed_2", text = "2", x = toolbarX+75, y = toolbarY+425, width = 0, height = headerHeight, font_r = 0, font_g = 0, font_b = 0, font_a = 255, font_id = "main", shadow_r = 0, shadow_g = 88, shadow_b = 227, shadow_a = 255, shadow_offset_x = 2, shadow_offset_y = 2, use_shadow = 1 },
    { page = 3, ui_type = "label",  id = "editor_speed_3", text = "3", x = toolbarX+125, y = toolbarY+425, width = 0, height = headerHeight, font_r = 0, font_g = 0, font_b = 0, font_a = 255, font_id = "main", shadow_r = 0, shadow_g = 88, shadow_b = 227, shadow_a = 255, shadow_offset_x = 2, shadow_offset_y = 2, use_shadow = 1 },
    { page = 3, ui_type = "label",  id = "editor_speed_4", text = "4", x = toolbarX+175, y = toolbarY+425, width = 0, height = headerHeight, font_r = 0, font_g = 0, font_b = 0, font_a = 255, font_id = "main", shadow_r = 0, shadow_g = 88, shadow_b = 227, shadow_a = 255, shadow_offset_x = 2, shadow_offset_y = 2, use_shadow = 1 },


    -- Page 4: Files
    { page = 4, ui_type = "image", id = "bg4", texture_id = "background", x = toolbarX, y = toolbarY, width = toolbarWidth, height = toolbarHeight },
    { page = 4, ui_type = "button", id = "minimize_editor_4", texture_id = "editor_minimize", x = 1000, y = 322, width = 23, height = 62 },

    { page = 4, ui_type = "button", id = "page4_goto1", texture_id = "editor_tabs", x = toolbarX+tabWidth*0, y = 0, width = tabWidth, height = tabHeight, frame_x = tabWidth*0, frame_y = tabHeight,    frame_width = tabWidth, frame_height = tabHeight },
    { page = 4, ui_type = "button", id = "page4_goto2", texture_id = "editor_tabs", x = toolbarX+tabWidth*1, y = 0, width = tabWidth, height = tabHeight, frame_x = tabWidth*1, frame_y = tabHeight,    frame_width = tabWidth, frame_height = tabHeight },
    { page = 4, ui_type = "button", id = "page4_goto3", texture_id = "editor_tabs", x = toolbarX+tabWidth*2, y = 0, width = tabWidth, height = tabHeight, frame_x = tabWidth*2, frame_y = tabHeight,    frame_width = tabWidth, frame_height = tabHeight },
    { page = 4, ui_type = "button", id = "page4_goto4", texture_id = "editor_tabs", x = toolbarX+tabWidth*3, y = 0, width = tabWidth, height = tabHeight, frame_x = tabWidth*3, frame_y = 0,            frame_width = tabWidth, frame_height = tabHeight },

    -- Textbox
    { page = 4, ui_type = "textbox", id = "property_mapname", x = toolbarX + 10, y = toolbarY + 40, width = 240, height = 30, font_id = "main", max_length = 50, text_height = 20, background_r = 0xFF, background_g = 0xFF, background_g = 0xFF, background_a = 0xFF, selected_r = 0xAA, selected_g = 0xAA, selected_g = 0xAA, selected_a = 0xFF, font_r = 0x00, font_g = 0x00, font_b = 0x00, font_a = 0xFF },

    -- Labels
    { page = 4, ui_type = "label",  id = "editor_filename",     text_id = "editor_filename",    x = toolbarX + 10, y = toolbarY + 10 + 000, width = 0, height = headerHeight, font_r = textr, font_g = textg, font_b = textb, font_a = 255, font_id = "main", shadow_r = 0, shadow_g = 88, shadow_b = 227, shadow_a = 255, shadow_offset_x = 2, shadow_offset_y = 2, use_shadow = 1 },
    { page = 4, ui_type = "label",  id = "editor_save_map",     text_id = "editor_save_map",    x = toolbarX + 75, y = toolbarY + 100,  width = 0, height = headerHeight, font_r = textr, font_g = textg, font_b = textb, font_a = 255, font_id = "main", shadow_r = 0, shadow_g = 88, shadow_b = 227, shadow_a = 255, shadow_offset_x = 2, shadow_offset_y = 2, use_shadow = 1 },
    { page = 4, ui_type = "label",  id = "editor_load_map",     text_id = "editor_load_map",    x = toolbarX + 75, y = toolbarY + 150, width = 0, height = headerHeight, font_r = textr, font_g = textg, font_b = textb, font_a = 255, font_id = "main", shadow_r = 0, shadow_g = 88, shadow_b = 227, shadow_a = 255, shadow_offset_x = 2, shadow_offset_y = 2, use_shadow = 1 },
    { page = 4, ui_type = "label",  id = "editor_test_level",   text_id = "editor_test_level",  x = toolbarX + 75, y = toolbarY + 200, width = 0, height = headerHeight, font_r = textr, font_g = textg, font_b = textb, font_a = 255, font_id = "main", shadow_r = 0, shadow_g = 88, shadow_b = 227, shadow_a = 255, shadow_offset_x = 2, shadow_offset_y = 2, use_shadow = 1 },
    { page = 4, ui_type = "label",  id = "editor_clear_level",  text_id = "editor_clear_level", x = toolbarX + 75, y = toolbarY + 10 + 390, width = 0, height = headerHeight, font_r = textr, font_g = textg, font_b = textb, font_a = 255, font_id = "main", shadow_r = 0, shadow_g = 88, shadow_b = 227, shadow_a = 255, shadow_offset_x = 2, shadow_offset_y = 2, use_shadow = 1 },

    -- Save / Load / Test / Clear
    { page = 4, ui_type = "button", id = "btn_save_map",    texture_id = "editor_file_buttons", x = toolbarX+10,    y = toolbarY+100, width = 40, height = 40, frame_x = 40,    frame_y = 0, frame_width = 40, frame_height = 40 },
    { page = 4, ui_type = "button", id = "btn_load_map",    texture_id = "editor_file_buttons", x = toolbarX+10,    y = toolbarY+150, width = 40, height = 40, frame_x = 0,     frame_y = 0, frame_width = 40, frame_height = 40 },
    { page = 4, ui_type = "button", id = "btn_test_map",    texture_id = "editor_file_buttons", x = toolbarX+10,    y = toolbarY+200, width = 40, height = 40, frame_x = 120,   frame_y = 0, frame_width = 40, frame_height = 40 },
    { page = 4, ui_type = "button", id = "btn_clear_map",   texture_id = "editor_file_buttons", x = toolbarX+10,    y = toolbarY+400, width = 40, height = 40, frame_x = 80,    frame_y = 0, frame_width = 40, frame_height = 40 },


    -- Page 5: Minimized
    { page = 5, ui_type = "button", id = "maximize_editor", texture_id = "editor_maximize", x = screenWidth - 23, y = 322, width = 23, height = 62 },
}
