assets = {
    { name = "background_art", path = "content/graphics/ui/mainmenu_background.png" },
    { name = "dolphin",        path = "content/graphics/ui/title-tilted-fin.png" },
    { name = "kitten",         path = "content/graphics/ui/title-tilted-kit.png" },
    { name = "title_text",     path = "content/graphics/ui/mainmenu_title.png" },
    { name = "button_main",    path = "content/graphics/ui/button_main.png" },
    { name = "button_sub",     path = "content/graphics/ui/button_sub.png" },
    { name = "button_fb",      path = "content/graphics/ui/button_social_facebook.png" },
    { name = "button_tw",      path = "content/graphics/ui/button_social_twitter.png" },
    { name = "button_left",    path = "content/graphics/ui/button_nav_left.png" },
    { name = "missing_in_build",    path = "content/graphics/ui/missing_in_build.png" },
    { name = "locked",         path = "content/graphics/ui/locked.png" }
}

menu_options = {
    total_pages = 3
}

button1y = 45
button2y = 220
button3y = 395



buttonx = 50
buttonx2 = 640
col2X = 760
scrw = 1280
scrh = 720

buttonw = 580
buttonh = 100

elements = {
    -- Common
    { ui_type = "image", id = "background", texture_id = "background_art",   x = 0, y = 0,   width = scrw, height = scrh },

    { ui_type = "image", id = "ddolphin", texture_id = "dolphin",         x = col2X-35,  y = 380,  width = 330, height = 300,   effect = "", effect_speed = 50 },
    { ui_type = "image", id = "ekitten", texture_id = "kitten",           x = col2X+270, y = 430,  width = 144, height = 127,   effect = "", effect_speed = 50 },
    { ui_type = "image", id = "ctitle_text", texture_id = "title_text",   x = col2X,     y = 20,   width = 400, height = 400,   effect = "",    effect_speed = 50 },

    -- Page 1: Title Screen
    { page = 1, ui_type = "button", id = "btn_play",    texture_id = "button_main",    x = buttonx, y = button1y,  width = buttonw, height = 100 },
    { page = 1, ui_type = "button", id = "btn_options", texture_id = "button_main",    x = buttonx, y = button1y+150,    width = buttonw, height = 100 },
    { page = 1, ui_type = "button", id = "btn_help",    texture_id = "button_main",    x = buttonx, y = button1y+300,    width = buttonw, height = 100 },
    { page = 1, ui_type = "button", id = "btn_exit",    texture_id = "button_main",    x = buttonx, y = button1y+450,    width = buttonw, height = 100 },

    { page = 1, ui_type = "button", id = "btn_moosader",    texture_id = "button_sub",     x = buttonx, y = scrh-90,        width = 250, height = 66 },
    { page = 1, ui_type = "button", id = "btn_fb",          texture_id = "button_fb",      x = buttonx+280, y = scrh-101,   width = 84, height = 84 },
    { page = 1, ui_type = "button", id = "btn_tw",          texture_id = "button_tw",      x = buttonx+400, y = scrh-101,   width = 84, height = 84 },

    { page = 1, ui_type = "label", id = "lbl_play",
        text_id = "mainmenu_play",    x = buttonx+10, y = button1y-5,   width = 0, height = buttonh, font_id = "main",
        font_r = 255, font_g = 255, font_b = 255, font_a = 255, shadow_r = 0, shadow_g = 88, shadow_b = 227, shadow_a = 255, shadow_offset_x = 2, shadow_offset_y = 2, use_shadow = 1  },
    { page = 1, ui_type = "label", id = "lbl_options",
        text_id = "mainmenu_options", x = buttonx+10, y = button1y+150-5,  width = 0, height = buttonh, font_id = "main",
        font_r = 255, font_g = 255, font_b = 255, font_a = 255, shadow_r = 0, shadow_g = 88, shadow_b = 227, shadow_a = 255, shadow_offset_x = 2, shadow_offset_y = 2, use_shadow = 1 },
    { page = 1, ui_type = "label", id = "lbl_help",
        text_id = "mainmenu_help",    x = buttonx+10, y = button1y+300-5,  width = 0, height = buttonh, font_id = "main",
        font_r = 255, font_g = 255, font_b = 255, font_a = 255, shadow_r = 0, shadow_g = 88, shadow_b = 227, shadow_a = 255, shadow_offset_x = 2, shadow_offset_y = 2, use_shadow = 1 },
    { page = 1, ui_type = "label", id = "lbl_exit",
        text_id = "mainmenu_exit",    x = buttonx+10, y = button1y+450-5,  width = 0, height = buttonh, font_id = "main",
        font_r = 255, font_g = 255, font_b = 255, font_a = 255, shadow_r = 0, shadow_g = 88, shadow_b = 227, shadow_a = 255, shadow_offset_x = 2, shadow_offset_y = 2, use_shadow = 1 },

    { page = 1, ui_type = "label", id = "lbl_moose",
        text_id = "mainmenu_moremoose", x = buttonx+10, y = scrh-80,  width = 250, height = 40, font_id = "main",
        font_r = 255, font_g = 255, font_b = 255, font_a = 255, shadow_r = 226, shadow_g = 85, shadow_b = 0, shadow_a = 255, shadow_offset_x = 2, shadow_offset_y = 2, use_shadow = 1 },

    -- Page 2: Options Menu

    -- Page 3: Play Menu
    { page = 3, ui_type = "button", id = "btn_story",  texture_id = "button_main", x = buttonx, y = button1y, width = buttonw, height = buttonh },
    { page = 3, ui_type = "button", id = "btn_editor", texture_id = "button_main", x = buttonx, y = button2y, width = buttonw, height = buttonh },
    { page = 3, ui_type = "button", id = "btn_custom", texture_id = "button_main", x = buttonx, y = button3y, width = buttonw, height = buttonh },

    { page = 3, ui_type = "button", id = "btn_back",  texture_id = "button_left", x = 20, y = scrh-20-107, width = 107, height = 107 },

    { page = 3, ui_type = "label", id = "lbl_story", text_id = "playmenu_story", x = buttonx+10, y = button1y-5, width = 0, height = buttonh,
        font_r = 255, font_g = 255, font_b = 255, font_a = 255, font_id = "main", shadow_r = 0, shadow_g = 88, shadow_b = 227, shadow_a = 255, shadow_offset_x = 2, shadow_offset_y = 2, use_shadow = 1 },

    { page = 3, ui_type = "label", id = "lbl_editor", text_id = "playmenu_editor", x = buttonx+10, y = button2y-5, width = 0, height = buttonh,
        font_r = 255, font_g = 255, font_b = 255, font_a = 255, font_id = "main", shadow_r = 0, shadow_g = 88, shadow_b = 227, shadow_a = 255, shadow_offset_x = 2, shadow_offset_y = 2, use_shadow = 1 },

    { page = 3, ui_type = "label", id = "lbl_custom", text_id = "playmenu_custom", x = buttonx+10, y = button3y-5,  width = 0, height = buttonh,
        font_r = 255, font_g = 255, font_b = 255, font_a = 255, font_id = "main", shadow_r = 0, shadow_g = 88, shadow_b = 227, shadow_a = 255, shadow_offset_x = 2, shadow_offset_y = 2, use_shadow = 1 },


}
