assets = {
    { name = "background_art", path = "content/graphics/ui/mainmenu_background.png" },
    { name = "button_main",    path = "content/graphics/ui/button_main.png" },
    { name = "button_sub",     path = "content/graphics/ui/button_sub.png" },
    { name = "button_fb",      path = "content/graphics/ui/button_social_facebook.png" },
    { name = "button_tw",      path = "content/graphics/ui/button_social_twitter.png" },
    { name = "button_left",    path = "content/graphics/ui/button_nav_left.png" },
    { name = "locked",         path = "content/graphics/ui/locked.png" },
    { name = "plus",           path = "content/graphics/ui/btn_plus.png" },
    { name = "minus",          path = "content/graphics/ui/btn_minus.png" },
    { name = "volume_slider",  path = "content/graphics/ui/volume_slider.png" },
    { name = "credits_background",  path = "content/graphics/ui/credits_background.png" },
    { name = "help_background",  path = "content/graphics/ui/help_background.png" },
    { name = "help_frame_1",   path = "content/graphics/ui/help_frame_1.png" },
    { name = "helper_obstacles",   path = "content/graphics/ui/helper_obstacles.png" },
    { name = "helper_trinkets",   path = "content/graphics/ui/helper_trinkets.png" },
    { name = "extra_heart",   path = "content/graphics/game/extra_heart.png" },
    { name = "threestars",   path = "content/graphics/ui/threestars.png" },
}

menu_options = {
    total_pages = 4
}

button1y = 45
button2y = 220
button3y = 395
buttonx = 50
buttonx2 = 690

buttonw = 580
buttonh = 100

creditsx = 200
creditsx2 = 650
creditsy = 50
creditsh = 30
inc = creditsh

helpx = 50
helpy = 20
helph = 30


elements = {
    { ui_type = "image", id = "background", texture_id = "background_art",   x = 0, y = 0,   width = scrw, height = scrh },

    -- Page 1: Options
    { page = 1, ui_type = "button", id = "btn_back1",  texture_id = "button_left", x = 20, y = scrh-20-107, width = 107, height = 107 },

    { page = 1, ui_type = "label", id = "lbl1", text_id = "options_sound_volume", x = buttonx+10, y = button1y-5, width = 0, height = buttonh,
        font_r = 255, font_g = 255, font_b = 255, font_a = 255, font_id = "main", shadow_r = 0, shadow_g = 88, shadow_b = 227, shadow_a = 255, shadow_offset_x = 2, shadow_offset_y = 2, use_shadow = 1 },

    { page = 1, ui_type = "button", id = "btn_minus_sound",     texture_id = "minus", x = buttonx2, y = button1y, width = 107, height = 107 },
    { page = 1, ui_type = "button", id = "btn_plus_sound",      texture_id = "plus", x = buttonx2+400, y = button1y, width = 107, height = 107 },

    { page = 1, ui_type = "button", id = "sound_slider", texture_id = "volume_slider",
        x = buttonx2+120, y = button1y+40, width = 270, height = 40, frame_x = 0, frame_y = 40*3, frame_width = 294, frame_height = 40,
        },


    { page = 1, ui_type = "label", id = "lbl2", text_id = "options_music_volume", x = buttonx+10, y = button2y-5, width = 0, height = buttonh,
        font_r = 255, font_g = 255, font_b = 255, font_a = 255, font_id = "main", shadow_r = 0, shadow_g = 88, shadow_b = 227, shadow_a = 255, shadow_offset_x = 2, shadow_offset_y = 2, use_shadow = 1 },

    { page = 1, ui_type = "button", id = "btn_minus_music",     texture_id = "minus", x = buttonx2, y = button2y, width = 107, height = 107 },
    { page = 1, ui_type = "button", id = "btn_plus_music",      texture_id = "plus", x = buttonx2+400, y = button2y, width = 107, height = 107 },

    { page = 1, ui_type = "button", id = "music_slider", texture_id = "volume_slider",
        x = buttonx2+120, y = button2y+40, width = 270, height = 40, frame_x = 0, frame_y = 40*3, frame_width = 294, frame_height = 40,
        },


    { page = 1, ui_type = "button", id = "btn_bg2",  texture_id = "button_main", x = buttonx, y = button1y, width = buttonw, height = buttonh },
    { page = 1, ui_type = "button", id = "btn_bg3",  texture_id = "button_main", x = buttonx, y = button2y, width = buttonw, height = buttonh },

    { page = 1, ui_type = "button", id = "btn_change_language",  texture_id = "button_main", x = buttonx, y = button3y, width = buttonw, height = buttonh },

    { page = 1, ui_type = "label", id = "lbl_language", text_id = "options_language", x = buttonx+10, y = button3y-5, width = 0, height = buttonh,
        font_r = 255, font_g = 255, font_b = 255, font_a = 255, font_id = "main", shadow_r = 0, shadow_g = 88, shadow_b = 227, shadow_a = 255, shadow_offset_x = 2, shadow_offset_y = 2, use_shadow = 1 },



    -- Page 2: Help
    { page = 2, ui_type = "button", id = "btn_back2",  texture_id = "button_left", x = 20, y = scrh-20-107, width = 107, height = 107 },

    { page = 2, ui_type = "button", id = "btn_how_to_play",  texture_id = "button_main", x = buttonx, y = button1y, width = buttonw, height = buttonh },
    { page = 2, ui_type = "button", id = "btn_credits",        texture_id = "button_main", x = buttonx, y = button2y, width = buttonw, height = buttonh },

    { page = 2, ui_type = "label", id = "lbl_how_to_play", text_id = "help_how_to_play", x = buttonx+10, y = button1y-5, width = 0, height = buttonh,
        font_r = 255, font_g = 255, font_b = 255, font_a = 255, font_id = "main", shadow_r = 0, shadow_g = 88, shadow_b = 227, shadow_a = 255, shadow_offset_x = 2, shadow_offset_y = 2, use_shadow = 1 },

    { page = 2, ui_type = "label", id = "lbl_credits", text_id = "help_credits", x = buttonx+10, y = button2y-5, width = 0, height = buttonh,
        font_r = 255, font_g = 255, font_b = 255, font_a = 255, font_id = "main", shadow_r = 0, shadow_g = 88, shadow_b = 227, shadow_a = 255, shadow_offset_x = 2, shadow_offset_y = 2, use_shadow = 1 },


    -- Page 3: How to play
    { page = 3, ui_type = "button", id = "btn_back3",  texture_id = "button_left", x = 20, y = scrh-20-107, width = 107, height = 107 },

    { page = 3, ui_type = "image", id = "background_howto", texture_id = "help_background",
        x = 0, y = 0, width = 1280, height = 720 },
    { page = 3, ui_type = "image", id = "zhelper_img", texture_id = "help_frame_1",
        x = helpx, y = helpy+inc*2, width = 400, height = 300 },

    { page = 3, ui_type = "label", id = "lblhowtoplay", text_id = "help_how_to_play",
        x = helpx, y = helpy, width = 0, height = helph+10, font_id = "main",
        font_r = 255, font_g = 255, font_b = 255, font_a = 255, shadow_r = 0, shadow_g = 88, shadow_b = 227, shadow_a = 255, shadow_offset_x = 2, shadow_offset_y = 2, use_shadow = 1 },


    { page = 3, ui_type = "label", id = "h1", text_id = "help_line1_pc",   x = helpx + 450, y = helpy+inc*1, width = 0, height = creditsh, font_id = "main", font_r = 255, font_g = 255, font_b = 255, font_a = 255, shadow_r = 0, shadow_g = 88, shadow_b = 227, shadow_a = 255, shadow_offset_x = 2, shadow_offset_y = 2, use_shadow = 1 },
    { page = 3, ui_type = "label", id = "h2", text_id = "help_line2_pc",   x = helpx + 450, y = helpy+inc*2, width = 0, height = creditsh, font_id = "main", font_r = 255, font_g = 255, font_b = 255, font_a = 255, shadow_r = 0, shadow_g = 88, shadow_b = 227, shadow_a = 255, shadow_offset_x = 2, shadow_offset_y = 2, use_shadow = 1 },
    --{ page = 3, ui_type = "label", id = "h2b", text_id = "help_line2b_pc", x = helpx + 500, y = helpy+inc*4, width = 0, height = creditsh, font_id = "main", font_r = 255, font_g = 255, font_b = 255, font_a = 255, shadow_r = 0, shadow_g = 88, shadow_b = 227, shadow_a = 255, shadow_offset_x = 2, shadow_offset_y = 2, use_shadow = 1 },
    { page = 3, ui_type = "label", id = "h3", text_id = "help_line3_pc",   x = helpx + 450, y = helpy+inc*4, width = 0, height = creditsh, font_id = "main", font_r = 255, font_g = 255, font_b = 255, font_a = 255, shadow_r = 0, shadow_g = 88, shadow_b = 227, shadow_a = 255, shadow_offset_x = 2, shadow_offset_y = 2, use_shadow = 1 },
    { page = 3, ui_type = "label", id = "h4", text_id = "help_line4_pc",   x = helpx + 450, y = helpy+inc*6, width = 0, height = creditsh, font_id = "main", font_r = 255, font_g = 255, font_b = 255, font_a = 255, shadow_r = 0, shadow_g = 88, shadow_b = 227, shadow_a = 255, shadow_offset_x = 2, shadow_offset_y = 2, use_shadow = 1 },
    --{ page = 3, ui_type = "label", id = "h4b", text_id = "help_line4b_pc", x = helpx + 500, y = helpy+inc*9, width = 0, height = creditsh, font_id = "main", font_r = 255, font_g = 255, font_b = 255, font_a = 255, shadow_r = 0, shadow_g = 88, shadow_b = 227, shadow_a = 255, shadow_offset_x = 2, shadow_offset_y = 2, use_shadow = 1 },
    { page = 3, ui_type = "label", id = "h5", text_id = "help_line5_pc",   x = helpx + 450, y = helpy+inc*8, width = 0, height = creditsh, font_id = "main", font_r = 255, font_g = 255, font_b = 255, font_a = 255, shadow_r = 0, shadow_g = 88, shadow_b = 227, shadow_a = 255, shadow_offset_x = 2, shadow_offset_y = 2, use_shadow = 1 },
    { page = 3, ui_type = "label", id = "h6", text_id = "help_line6_pc",   x = helpx + 450, y = helpy+inc*9, width = 0, height = creditsh, font_id = "main", font_r = 255, font_g = 255, font_b = 255, font_a = 255, shadow_r = 0, shadow_g = 88, shadow_b = 227, shadow_a = 255, shadow_offset_x = 2, shadow_offset_y = 2, use_shadow = 1 },
    { page = 3, ui_type = "label", id = "h9", text_id = "help_line9_pc",   x = helpx + 500, y = helpy+inc*11, width = 0, height = creditsh, font_id = "main",        font_r = 255, font_g = 255, font_b = 255, font_a = 255, shadow_r = 0, shadow_g = 88, shadow_b = 227, shadow_a = 255, shadow_offset_x = 2, shadow_offset_y = 2, use_shadow = 1 },

    { page = 3, ui_type = "image", id = "heart", texture_id = "extra_heart", x = helpx+425,      y = helpy+inc*11-20, width = 80, height = 80 },


    { page = 3, ui_type = "image", id = "zhelper_obstacles", texture_id = "helper_obstacles",        x = helpx+350,      y = helpy+inc*16, width = 400, height = 200 },
    { page = 3, ui_type = "image", id = "zhelper_trinkets", texture_id = "helper_trinkets",        x = helpx+800,  y = helpy+inc*16, width = 400, height = 200 },

    { page = 3, ui_type = "label", id = "h7", text_id = "help_line7_pc",        x = helpx+800,  y = helpy+inc*15, width = 0, height = creditsh, font_id = "main",        font_r = 255, font_g = 255, font_b = 255, font_a = 255, shadow_r = 0, shadow_g = 88, shadow_b = 227, shadow_a = 255, shadow_offset_x = 2, shadow_offset_y = 2, use_shadow = 1 },
    { page = 3, ui_type = "label", id = "h8", text_id = "help_line8_pc",        x = helpx+350,  y = helpy+inc*15, width = 0, height = creditsh, font_id = "main",        font_r = 255, font_g = 255, font_b = 255, font_a = 255, shadow_r = 0, shadow_g = 88, shadow_b = 227, shadow_a = 255, shadow_offset_x = 2, shadow_offset_y = 2, use_shadow = 1 },

    { page = 3, ui_type = "label", id = "h10", text_id = "help_line10_pc",      x = helpx+130, y = helpy+inc*13, width = 0, height = creditsh, font_id = "main",        font_r = 255, font_g = 255, font_b = 255, font_a = 255, shadow_r = 0, shadow_g = 88, shadow_b = 227, shadow_a = 255, shadow_offset_x = 2, shadow_offset_y = 2, use_shadow = 1 },

    { page = 3, ui_type = "image", id = "threestars", texture_id = "threestars", x = helpx,      y = helpy+inc*13-5, width = 120, height = 40 },



    -- Page 4: Credits
    { page = 4, ui_type = "button", id = "btn_back4",  texture_id = "button_left", x = 20, y = scrh-20-107, width = 107, height = 107 },

    { page = 4, ui_type = "image", id = "credits_background", texture_id = "credits_background",
        x = 175, y = 40, width = 1068, height = 613 },

    { page = 4, ui_type = "label", id = "lblcredits", text_id = "credits_moosader_team",
        x = creditsx, y = creditsy, width = 0, height = creditsh+10, font_id = "main",
        font_r = 255, font_g = 255, font_b = 255, font_a = 255, shadow_r = 0, shadow_g = 88, shadow_b = 227, shadow_a = 255, shadow_offset_x = 2, shadow_offset_y = 2, use_shadow = 1 },


    { page = 4, ui_type = "label", id = "c1", text_id = "credits_name_racheljmorris",
        x = creditsx, y = creditsy+inc*2, width = 0, height = creditsh, font_id = "main",
        font_r = 255, font_g = 255, font_b = 255, font_a = 255, shadow_r = 0, shadow_g = 88, shadow_b = 227, shadow_a = 255, shadow_offset_x = 2, shadow_offset_y = 2, use_shadow = 1 },
    { page = 4, ui_type = "label", id = "c2", text_id = "credits_role_racheljmorris",
        x = creditsx, y = creditsy+inc*3, width = 0, height = creditsh, font_id = "main",
        font_r = 255, font_g = 255, font_b = 255, font_a = 255, shadow_r = 0, shadow_g = 88, shadow_b = 227, shadow_a = 255, shadow_offset_x = 2, shadow_offset_y = 2, use_shadow = 1 },


    { page = 4, ui_type = "label", id = "lblcredits3", text_id = "credits_name_enmanueltoribio",
        x = creditsx, y = creditsy+inc*5, width = 0, height = creditsh, font_id = "main",
        font_r = 255, font_g = 255, font_b = 255, font_a = 255, shadow_r = 0, shadow_g = 88, shadow_b = 227, shadow_a = 255, shadow_offset_x = 2, shadow_offset_y = 2, use_shadow = 1 },
    { page = 4, ui_type = "label", id = "lblcredits4", text_id = "credits_role_enmanueltoribio",
        x = creditsx, y = creditsy+inc*6, width = 0, height = creditsh, font_id = "main",
        font_r = 255, font_g = 255, font_b = 255, font_a = 255, shadow_r = 0, shadow_g = 88, shadow_b = 227, shadow_a = 255, shadow_offset_x = 2, shadow_offset_y = 2, use_shadow = 1 },


    { page = 4, ui_type = "label", id = "c5", text_id = "credits_name_jessicacapehart",
        x = creditsx, y = creditsy+inc*8, width = 0, height = creditsh, font_id = "main",
        font_r = 255, font_g = 255, font_b = 255, font_a = 255, shadow_r = 0, shadow_g = 88, shadow_b = 227, shadow_a = 255, shadow_offset_x = 2, shadow_offset_y = 2, use_shadow = 1 },
    { page = 4, ui_type = "label", id = "c6", text_id = "credits_role_jessicacapehart",
        x = creditsx, y = creditsy+inc*9, width = 0, height = creditsh, font_id = "main",
        font_r = 255, font_g = 255, font_b = 255, font_a = 255, shadow_r = 0, shadow_g = 88, shadow_b = 227, shadow_a = 255, shadow_offset_x = 2, shadow_offset_y = 2, use_shadow = 1 },


    { page = 4, ui_type = "label", id = "c7", text_id = "credits_name_rebekahhamilton",
        x = creditsx, y = creditsy+inc*11, width = 0, height = creditsh, font_id = "main",
        font_r = 255, font_g = 255, font_b = 255, font_a = 255, shadow_r = 0, shadow_g = 88, shadow_b = 227, shadow_a = 255, shadow_offset_x = 2, shadow_offset_y = 2, use_shadow = 1 },
    { page = 4, ui_type = "label", id = "c8", text_id = "credits_role_rebekahhamilton",
        x = creditsx, y = creditsy+inc*12, width = 0, height = creditsh, font_id = "main",
        font_r = 255, font_g = 255, font_b = 255, font_a = 255, shadow_r = 0, shadow_g = 88, shadow_b = 227, shadow_a = 255, shadow_offset_x = 2, shadow_offset_y = 2, use_shadow = 1 },


    { page = 4, ui_type = "label", id = "c9", text_id = "credits_name_rosemorris",
        x = creditsx, y = creditsy+inc*14, width = 0, height = creditsh, font_id = "main",
        font_r = 255, font_g = 255, font_b = 255, font_a = 255, shadow_r = 0, shadow_g = 88, shadow_b = 227, shadow_a = 255, shadow_offset_x = 2, shadow_offset_y = 2, use_shadow = 1 },
    { page = 4, ui_type = "label", id = "c10", text_id = "credits_role_rosemorris",
        x = creditsx, y = creditsy+inc*15, width = 0, height = creditsh, font_id = "main",
        font_r = 255, font_g = 255, font_b = 255, font_a = 255, shadow_r = 0, shadow_g = 88, shadow_b = 227, shadow_a = 255, shadow_offset_x = 2, shadow_offset_y = 2, use_shadow = 1 },


    { page = 4, ui_type = "label", id = "c3", text_id = "credits_name_teacoba",
        x = creditsx, y = creditsy+inc*17, width = 0, height = creditsh, font_id = "main",
        font_r = 255, font_g = 255, font_b = 255, font_a = 255, shadow_r = 0, shadow_g = 88, shadow_b = 227, shadow_a = 255, shadow_offset_x = 2, shadow_offset_y = 2, use_shadow = 1 },
    { page = 4, ui_type = "label", id = "c4", text_id = "credits_role_teacoba",
        x = creditsx, y = creditsy+inc*18, width = 0, height = creditsh, font_id = "main",
        font_r = 255, font_g = 255, font_b = 255, font_a = 255, shadow_r = 0, shadow_g = 88, shadow_b = 227, shadow_a = 255, shadow_offset_x = 2, shadow_offset_y = 2, use_shadow = 1 },



    { page = 4, ui_type = "label", id = "lblcredits2", text_id = "credits_other",
        x = creditsx2, y = creditsy, width = 0, height = creditsh+10, font_id = "main",
        font_r = 255, font_g = 255, font_b = 255, font_a = 255, shadow_r = 0, shadow_g = 88, shadow_b = 227, shadow_a = 255, shadow_offset_x = 2, shadow_offset_y = 2, use_shadow = 1 },


    { page = 4, ui_type = "label", id = "co1", text_id = "credits_sound_effects_a",
        x = creditsx2, y = creditsy+inc*2, width = 0, height = creditsh, font_id = "main",
        font_r = 255, font_g = 255, font_b = 255, font_a = 255, shadow_r = 0, shadow_g = 88, shadow_b = 227, shadow_a = 255, shadow_offset_x = 2, shadow_offset_y = 2, use_shadow = 1 },
    { page = 4, ui_type = "label", id = "co2", text_id = "credits_sound_effects_b",
        x = creditsx2, y = creditsy+inc*3, width = 0, height = creditsh, font_id = "main",
        font_r = 255, font_g = 255, font_b = 255, font_a = 255, shadow_r = 0, shadow_g = 88, shadow_b = 227, shadow_a = 255, shadow_offset_x = 2, shadow_offset_y = 2, use_shadow = 1 },



    { page = 4, ui_type = "label", id = "co3", text_id = "credits_music_a",
        x = creditsx2, y = creditsy+inc*5, width = 0, height = creditsh, font_id = "main",
        font_r = 255, font_g = 255, font_b = 255, font_a = 255, shadow_r = 0, shadow_g = 88, shadow_b = 227, shadow_a = 255, shadow_offset_x = 2, shadow_offset_y = 2, use_shadow = 1 },
    { page = 4, ui_type = "label", id = "co4", text_id = "credits_music_b",
        x = creditsx2, y = creditsy+inc*6, width = 0, height = creditsh, font_id = "main",
        font_r = 255, font_g = 255, font_b = 255, font_a = 255, shadow_r = 0, shadow_g = 88, shadow_b = 227, shadow_a = 255, shadow_offset_x = 2, shadow_offset_y = 2, use_shadow = 1 },
    { page = 4, ui_type = "label", id = "co5", text_id = "credits_music_c",
        x = creditsx2, y = creditsy+inc*7, width = 0, height = creditsh, font_id = "main",
        font_r = 255, font_g = 255, font_b = 255, font_a = 255, shadow_r = 0, shadow_g = 88, shadow_b = 227, shadow_a = 255, shadow_offset_x = 2, shadow_offset_y = 2, use_shadow = 1 },
    { page = 4, ui_type = "label", id = "co6", text_id = "credits_music_c",
        x = creditsx2, y = creditsy+inc*7, width = 0, height = creditsh, font_id = "main",
        font_r = 255, font_g = 255, font_b = 255, font_a = 255, shadow_r = 0, shadow_g = 88, shadow_b = 227, shadow_a = 255, shadow_offset_x = 2, shadow_offset_y = 2, use_shadow = 1 },

{ page = 4, ui_type = "label", id = "co7", text_id = "credits_music_d",
        x = creditsx2+10, y = creditsy+inc*8, width = 0, height = creditsh, font_id = "main",
        font_r = 255, font_g = 255, font_b = 255, font_a = 255, shadow_r = 0, shadow_g = 88, shadow_b = 227, shadow_a = 255, shadow_offset_x = 2, shadow_offset_y = 2, use_shadow = 1 },
{ page = 4, ui_type = "label", id = "co8", text_id = "credits_music_e",
        x = creditsx2+10, y = creditsy+inc*9, width = 0, height = creditsh, font_id = "main",
        font_r = 255, font_g = 255, font_b = 255, font_a = 255, shadow_r = 0, shadow_g = 88, shadow_b = 227, shadow_a = 255, shadow_offset_x = 2, shadow_offset_y = 2, use_shadow = 1 },
{ page = 4, ui_type = "label", id = "co9", text_id = "credits_music_f",
        x = creditsx2+10, y = creditsy+inc*10, width = 0, height = creditsh, font_id = "main",
        font_r = 255, font_g = 255, font_b = 255, font_a = 255, shadow_r = 0, shadow_g = 88, shadow_b = 227, shadow_a = 255, shadow_offset_x = 2, shadow_offset_y = 2, use_shadow = 1 },
{ page = 4, ui_type = "label", id = "co10", text_id = "credits_music_g",
        x = creditsx2+10, y = creditsy+inc*11, width = 0, height = creditsh, font_id = "main",
        font_r = 255, font_g = 255, font_b = 255, font_a = 255, shadow_r = 0, shadow_g = 88, shadow_b = 227, shadow_a = 255, shadow_offset_x = 2, shadow_offset_y = 2, use_shadow = 1 },
{ page = 4, ui_type = "label", id = "co11", text_id = "credits_music_h",
        x = creditsx2+10, y = creditsy+inc*12, width = 0, height = creditsh, font_id = "main",
        font_r = 255, font_g = 255, font_b = 255, font_a = 255, shadow_r = 0, shadow_g = 88, shadow_b = 227, shadow_a = 255, shadow_offset_x = 2, shadow_offset_y = 2, use_shadow = 1 },

    { page = 4, ui_type = "label", id = "co12", text_id = "credits_font_a",
        x = creditsx2, y = creditsy+inc*14, width = 0, height = creditsh, font_id = "main",
        font_r = 255, font_g = 255, font_b = 255, font_a = 255, shadow_r = 0, shadow_g = 88, shadow_b = 227, shadow_a = 255, shadow_offset_x = 2, shadow_offset_y = 2, use_shadow = 1 },
    { page = 4, ui_type = "label", id = "co13", text_id = "credits_font_b",
        x = creditsx2, y = creditsy+inc*15, width = 0, height = creditsh, font_id = "main",
        font_r = 255, font_g = 255, font_b = 255, font_a = 255, shadow_r = 0, shadow_g = 88, shadow_b = 227, shadow_a = 255, shadow_offset_x = 2, shadow_offset_y = 2, use_shadow = 1 },

}
