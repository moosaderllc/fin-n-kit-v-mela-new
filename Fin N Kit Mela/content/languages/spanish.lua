if ( language == nil ) then language = {} end

language = {
	suggested_font = "NotoSans-Bold.ttf",

	language = "Español",

	game_title = "Fin 'n' Kit",
	game_creator = "Moosader LLC",
	game_website = "Moosader.com",
	game_year = "2016",
	game_version = "Dev Version 2016-08-08",

	game_title_1 = "Fin",
	game_title_2 = "'n'",
	game_title_3 = "Kit",

	mainmenu_play = "Jugar",
	mainmenu_exit = "Salir",
	mainmenu_options = "Opciones",
	mainmenu_help = "Ayuda",
	mainmenu_moremoose = "¡Más Moosader!",

	menu_tiles = "Tiles",
	menu_items = "Objetos",
	menu_select = "Seleccionar",
	menu_options2 = "Opciones",

	playmenu_story = "Modo historia",
	playmenu_editor = "Editor de niveles",
	playmenu_custom = "Niveles personalizados",

    world_1 = "Mundo 1",
    world_2 = "Mundo 2",
    world_3 = "Mundo 3",
    world_4 = "Mundo 4",
    world_5 = "Mundo 5",

    world_1_name = "Playa Agradable",
    world_2_name = "Mundo Misterioso",
    world_3_name = "Mundo Misterioso",
    world_4_name = "Mundo Misterioso",
    world_5_name = "Mundo Misterioso",

    help_how_to_play = "Cómo jugar",
    help_credits = "Créditos",

    options_sound_volume = "Volumen del sonido",
    options_music_volume = "Volumen de la música",
    options_language = "Cambiar idioma",

	editor_map_name = "Nombre del nivel",
	editor_save_map = "Guardar",
	editor_saved = "Guardado: ",
	editor_load_map = "Cargar",
	editor_load_map_name = "default.fin",
	editor_map_properties = "Propiedades",
	editor_file_mgmt = "Gestión de archivos",
	editor_clear_map = "Limpiar mapa",
	editor_back = "Salir",
	editor_confirm_exit = "Salir del editor",
	editor_theme = "Tema",
	editor_theme_afternoon = "Tarde",
	editor_theme_sunset = "Anochecer",
    editor_trinkets = "Baratijas",
    editor_obstacles = "Obstáculos",
    editor_misc_tiles = "Tiles miscelaneos",
    editor_extra_life = "Golpe extra",
    editor_level_end = "Fin del nivel",
    editor_filename = "Nombre del archivo",
    editor_background = "Fondo",
    editor_ground = "Terreno",
    editor_scroll = "Velocidad de scroll",
    editor_clear_map = "Limpiar mapa",
    editor_extra_life = "Vida extra",
    editor_level_end = "Marcador de fin de nivel",
    editor_test_level = "Probar",
    editor_music = "Música",
    editor_clear_level = "Limpio",
    editor_behavior = "Comportamiento",
    editor_scale = "Escala",
    editor_song_1 = "Canción 1",
    editor_song_2 = "Canción 2",
    editor_song_3 = "Canción 3",
    editor_scale_50 = "50%",
    editor_scale_100 = "100%",
    editor_scale_150 = "150%",
    editor_status_hello = "¡Hola!",
    editor_status_set_default_behavior = "Comportamiento actualizado",
    editor_status_set_default_scale = "Comportamiento por defecto actualizado",
    editor_status_set_tile_behavior = "Comportamiento: ",
    editor_status_set_tile_scale = "Escala cambiada: ",
    editor_status_added_new_tile = "Añadir tile: ",
    editor_status_removed_tile = "Borrar tiles",
    editor_status_tile_count = "Total de tiles: ",
    editor_status_current_brush = "Pincel actual: ",
    editor_status_updated_background = "Fondo actualizado",
    editor_status_updated_ground = "Terreno actualizado",
    editor_status_updated_song = "Canción actualizada",
    editor_status_updated_speed = "Velocidad de scroll actualizada",

    game_menu_success = "¡Exito!",
    game_menu_failure = "¡Fracaso!",
    game_menu_paused = "Pausa",
    game_menu_exit = "Salir",
    game_menu_restart = "Reiniciar",
    game_menu_continue = "Continuar",
    game_menu_next_level = "Siguiente nivel",

    credits_moosader_team = "El equipo de Moosader",
    credits_other = "Otros recursos",

    credits_name_racheljmorris = "Rachel Jane Morris",
    credits_role_racheljmorris = "Programadora líder, artista líder",

    credits_name_teacoba = "Tea Coba",
    credits_role_teacoba = "Diseño de niveles, traducción",

    credits_name_jessicacapehart = "Jessica Capehart",
    credits_role_jessicacapehart = "Marketing, medios",

    credits_name_rebekahhamilton = "Rebekah Hamilton",
    credits_role_rebekahhamilton = "Diseño de niveles",

    credits_name_enmanueltoribio = "Enmanuel Toribio",
    credits_role_enmanueltoribio = "Programación",

    credits_name_rosemorris = "Rose Morris",
    credits_role_rosemorris = "Medios",

    credits_sound_effects_a = "Efectos de sonido TEMPORARIOS",
    credits_sound_effects_b = "BFXR.NET",

    credits_music_a = "TEMPORARY Music",
    credits_music_b = "Kevin MacLeod",
    credits_music_c = "Incompetech.com",
    credits_music_d = "Carnivale Intrigue",
    credits_music_e = "Mariachi Snooze",
    credits_music_f = "Motherlode",
    credits_music_g = "Thief In The Night",
    credits_music_h = "Upbeat Forever",

    credits_font_a = "Fuente",
    credits_font_b = "Google Noto Sans, OFL",

    help_line1_mobile = "Toca sobre Kit para hacerla saltar.",
    help_line2_mobile = "Toca de nuevo mientras Kit está en el aire para realizar un doble salto.",
    help_line3_mobile = "Toca sobre Fin para hacerle saltar mientras Kit está en el aire.",
    help_line4_mobile = "Si Kit está sentada sobre Fin, toca sobre Fin para hacer saltar a Kit.",
    help_line5_mobile = "",
    help_line6_mobile = "",
    help_line7_mobile = "Recoge baratijas y evita los obstáculos.",
    help_line8_mobile = "Recoge baratijas y evita hacerte daño para obtener 3 estrellas.",
    help_line9_mobile = "Reune corazones para obtener un golpe extra.",

    help_line1_pc = "Haz click sobre Kit para hacerla saltar.",
    help_line2_pc = "Haz click de nuevo mientras Kit está en el aire para realizar un doble salto.",
    help_line2b_pc = "",

    help_line3_pc = "Haz click sobre Fin para hacerle saltar mientras Kit está en el aire.",
    help_line4_pc = "Si Kit está sentada sobre Fin, haz click sobre Fin para hacer saltar a Kit.",
    help_line4b_pc = "",

    help_line5_pc = "También puedes usar las teclas "\ARRIBA\" y "\ABAJO\" ",
    help_line6_pc = "para hacer saltar a Fin y a Kit.",
    help_line7_pc = "¡Recoge baratijas!",
    help_line8_pc = "¡Evita los obstáculos!",
    help_line9_pc = "Reune corazones para obtener un golpe extra .",
    help_line10_pc = "Consigue 3 estrellas en cada nivel recogiendo baratijas y evitando el peligro.",
    help_line10b_pc = "",

    cutscene_w1_c1a = "En una playa cálida y soleada",
    cutscene_w1_c1b = "En un día cálido y soleado",
    cutscene_w1_c1c = "La gatita Kit sale a pescar.",

    cutscene_w1_c2a = "Al ser todavía muy joven",
    cutscene_w1_c2b = "no se le da bien,",
    cutscene_w1_c2c = "pero ella es tenaz y no sabe cuando rendirse.",

    cutscene_w1_c3a = "Tras varias horas de práctica...",
    cutscene_w1_c5a = "...¡Kit obtiene su premio!",
    cutscene_w1_c6a = "(¡Con un poco de ayuda!)",

    cutscene_w1_c7a = "Fin dice \"¡Hola!\"",
    cutscene_w1_c7b = "y Kit contesta \"¡Buenas!\"",

    cutscene_w1_c8a = "Y una nueva amistad surgió...",
    cutscene_w1_c8b = "...basada en su amor común por el pescado",

    cutscene_w1_c9a = "Fin ofrece llevar a Kit en busca de más pescado.",
    cutscene_w1_c9b = "Kit se sube entusiasmada.",

    cutscene_w1_c10a = "¡Allá vamos, pescado!",
}

-- [[] Dedicado a : Julie, Rachel, Jessica, Enmanuel, Rebekah, Rose and all our pets.
       Ser parte de Moosader para mí es una de las mayores alegrías que tengo en esta vida.
       Gracias a este maravilloso equipo he encontrado a grandes amigos y ilusión por vivir.
			 Espero que todo aquel que juegue a esta nuestra segunda gran aventura pueda llevarse
			 un poco del cariño que hemos vertido en realizarla.

       Moose love: Tea.]]
