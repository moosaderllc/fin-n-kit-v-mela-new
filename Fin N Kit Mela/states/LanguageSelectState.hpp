#ifndef LANGUAGE_STATE_HPP
#define LANGUAGE_STATE_HPP

#include "../Kuko/states/IState.hpp"
#include "../Kuko/managers/MenuManager.hpp"

#include <vector>
#include <string>

class LanguageSelectState : public kuko::IState
{
    public:
    LanguageSelectState( const std::string& contentPath );
    virtual ~LanguageSelectState();

    virtual void Setup();
    virtual void Cleanup();
    virtual void Update();
    virtual void Draw();

    virtual void SetContentPath( const std::string& path );

    private:
    kuko::MenuManager menuManager;

    void SetupMenu();

    std::string m_state;

    std::string m_contentPath;
};

#endif
