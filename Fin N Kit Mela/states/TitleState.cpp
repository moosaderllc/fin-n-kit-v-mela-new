#include "TitleState.hpp"
#include <cstdio>
#include <cstdlib>

#include "../Kuko/managers/ImageManager.hpp"
#include "../Kuko/managers/InputManager.hpp"
#include "../Kuko/managers/FontManager.hpp"
#include "../Kuko/managers/LanguageManager.hpp"
#include "../Kuko/managers/SoundManager.hpp"
#include "../Kuko/managers/ConfigManager.hpp"
#include "../Kuko/utilities/Logger.hpp"
#include "../Kuko/base/Application.hpp"
#include "../Kuko/utilities/StringUtil.hpp"
#include "../Kuko/utilities/Platform.hpp"
#include "../utils/Messager.hpp"

TitleState::~TitleState()
{
}

TitleState::TitleState( const std::string& contentPath ) : IState()
{
    SetContentPath( contentPath );
}

void TitleState::Setup()
{
    m_state = "";
    kuko::IState::Setup();
    kuko::SoundManager::PlayMusic( "title", true );

    // Clear the stack state
    while ( m_stateStack.size() > 0 )
    {
        m_stateStack.pop();
    }

    if ( Messager::msg["title_state"] == "play_levelselect" )
    {
        m_stateStack.push( "title" );
        m_stateStack.push( "play" );
        m_stateStack.push( "play_story" );
        Goto_Play_LevelSelect( 1 );
    }
    else if ( Messager::msg["title_state"] == "play" )
    {
        m_stateStack.push( "title" );
        Goto_Play();
    }
    else
    {
        Goto_Title();
    }
}

void TitleState::Cleanup()
{
    kuko::ImageManager::ClearTextures();
}

void TitleState::SetContentPath( const std::string& path )
{
    m_contentPath = path;
}

void TitleState::Update()
{
    kuko::InputManager::Update();
    std::map<kuko::CommandButton, kuko::TriggerInfo> input = kuko::InputManager::GetTriggerInfo();

    if ( input[ kuko::WINDOW_CLOSE ].down )
    {
        m_gotoState = "quit";
    }
    else if ( input[ kuko::TAP ].down )
    {
        int x = input[ kuko::TAP ].actionX, y = input[ kuko::TAP ].actionY;
        std::string clickedButtonId = menuManager.GetButtonClicked( x, y );

        if ( clickedButtonId.find( "btn_back" ) != std::string::npos )
        {
            GoBack();
        }
        else if ( m_state == "title" )
        {
            Update_Title( clickedButtonId );
        }
        else if ( m_state == "play" )
        {
            Update_Play( clickedButtonId );
        }
        else if ( m_state == "play_story" )
        {
            Update_Play_Story( clickedButtonId );
        }
        else if ( m_state == "play_levelselect" )
        {
            Update_Play_LevelSelect( clickedButtonId );
        }
        else if ( m_state == "options" )
        {
            Update_Options( clickedButtonId );
        }
        else if ( m_state == "help" )
        {
            Update_Help( clickedButtonId );
        }
        else if ( m_state == "help_howtoplay" )
        {
            Update_Help_HowToPlay( clickedButtonId );
        }
        else if ( m_state == "help_credits" )
        {
            Update_Help_Credits( clickedButtonId );
        }
    }

    menuManager.Update();
}

void TitleState::Update_Title( const std::string& clickedButtonId )
{
    if( clickedButtonId == "btn_play" )
    {
        kuko::SoundManager::PlaySound( "button_confirm" );
        Goto_Play();
    }

    else if ( clickedButtonId == "btn_options" )
    {
        kuko::SoundManager::PlaySound( "button_confirm" );
        Goto_Options();
    }

    else if ( clickedButtonId == "btn_help" )
    {
        kuko::SoundManager::PlaySound( "button_confirm" );
        Goto_Help();
    }

    else if ( clickedButtonId == "btn_moosader" )
    {
        Platform::OpenUrl( "http://www.moosader.com" );
    }
    else if( clickedButtonId == "btn_fb" )
    {
        Platform::OpenUrl( "http://facebook.com/moosader" );
    }
    else if( clickedButtonId == "btn_tw" )
    {
        Platform::OpenUrl( "http://twitter.com/moosader" );
    }
    else if ( clickedButtonId == "btn_exit" )
    {
        m_gotoState = "quit";
    }
}

void TitleState::Update_Play( const std::string& clickedButtonId )
{
    if ( clickedButtonId == "btn_story" )
    {
        kuko::SoundManager::PlaySound( "button_confirm" );
        Goto_Play_Story();
    }
    else if ( clickedButtonId == "btn_editor" )
    {
        Messager::msg["title_state"] = m_state;
        kuko::SoundManager::PlaySound( "button_confirm" );
        m_gotoState = "level_editor";
    }

    else if ( clickedButtonId == "btn_custom" )
    {
        Messager::msg["title_state"] = m_state;
        kuko::SoundManager::PlaySound( "button_confirm" );
        m_gotoState = "custom_level_select";
    }
}

void TitleState::Update_Play_Story( const std::string& clickedButtonId )
{
    if ( clickedButtonId == "btn_world_1" )
    {
        kuko::SoundManager::PlaySound( "button_confirm" );
        Goto_Play_LevelSelect( 1 );
    }
    else if ( clickedButtonId == "btn_prev_world" )
    {
        kuko::SoundManager::PlaySound( "button_confirm" );
        int prev = menuManager.GetCurrentPage() - 1;
        if ( prev == 0 ) { prev = 5; }
        menuManager.SetCurrentPage( prev );
    }

    else if ( clickedButtonId == "btn_next_world" )
    {
        kuko::SoundManager::PlaySound( "button_confirm" );
        int next = menuManager.GetCurrentPage() + 1;
        if ( next == 6 ) { next = 1; }
        menuManager.SetCurrentPage( next );
    }
}

void TitleState::Update_Play_LevelSelect( const std::string& clickedButtonId )
{
    if ( clickedButtonId == "btn_help" )
    {
        kuko::SoundManager::PlaySound( "button_confirm" );
        Goto_Help_HowToPlay();
    }
    else if ( clickedButtonId == "btn_w1_cut" )
    {
        kuko::SoundManager::PlaySound( "button_confirm" );
        Messager::msg["title_state"] = m_state;
        m_gotoState = "cutscene";
    }
    else if ( clickedButtonId.find( "playlevel" ) != std::string::npos )
    {
        Logger::Debug( "Clicked level " + clickedButtonId );
        std::string mapName = clickedButtonId.substr( 10, 5 );
        Messager::msg["level"] = mapName + ".fin";
        Messager::msg["title_state"] = m_state;
        Messager::msg["previousState"] = "title";
        m_gotoState = "juggle_game";
    }
}

void TitleState::Update_Options( const std::string& clickedButtonId )
{
    if ( clickedButtonId == "btn_change_language" )
    {
        kuko::SoundManager::PlaySound( "button_confirm" );
        kuko::LanguageManager::AddLanguage( "common", m_contentPath + "languages/common.lua" );
        m_gotoState = "language_select";
    }
    else if ( clickedButtonId.find( "plus" ) != std::string::npos || clickedButtonId.find( "minus" ) != std::string::npos )
    {
        AdjustVolume( clickedButtonId );
    }
}

void TitleState::Update_Help( const std::string& clickedButtonId )
{
    if ( clickedButtonId == "btn_how_to_play" )
    {
        kuko::SoundManager::PlaySound( "button_confirm" );
        Goto_Help_HowToPlay();
    }

    else if ( clickedButtonId == "btn_credits" )
    {
        kuko::SoundManager::PlaySound( "button_confirm" );
        Goto_Help_Credits();
    }
}

void TitleState::Update_Help_HowToPlay( const std::string& clickedButtonId )
{
}

void TitleState::Update_Help_Credits( const std::string& clickedButtonId )
{
}

void TitleState::GoBack()
{
    Logger::Debug( "Go back", "TitleState::GoBack" );
    if ( m_stateStack.size() < 2 ) { return; }

    kuko::SoundManager::PlaySound( "button_cancel" );
    m_stateStack.pop();
    std::string previousState = m_stateStack.top();
    m_stateStack.pop();

    Logger::Debug( "go to " + previousState, "TitleState::GoBack" );
    if ( previousState == "title" )
    {
        Goto_Title();
    }
    else if ( previousState == "play" )
    {
        Goto_Play();
    }
    else if ( previousState == "play_story" )
    {
        Goto_Play_Story();
    }
    else if ( previousState == "play_levelselect" )
    {
        Goto_Play_LevelSelect( 1 );
    }
    else if ( previousState == "options" )
    {
        Goto_Options();
    }
    else if ( previousState == "help" )
    {
        Goto_Help();
    }
    else if ( previousState == "help_howtoplay" )
    {
        Goto_Help_HowToPlay();
    }
    else if ( previousState == "help_credits" )
    {
        Goto_Help_Credits();
    }
}

void TitleState::Goto_Title()
{
    SetupMenu_TitleScreen();
    menuManager.SetCurrentPage( 1 );
    m_state = "title";
    m_stateStack.push( m_state );
    Logger::Debug( "Push " + m_state, "GOTO" );
}

void TitleState::Goto_Play()
{
    SetupMenu_TitleScreen();
    menuManager.SetCurrentPage( 3 );
    m_state = "play";
    m_stateStack.push( m_state );
    Logger::Debug( "Push " + m_state, "GOTO" );
}

void TitleState::Goto_Play_Story()
{
    SetupMenu_StoryScreen();
    menuManager.SetCurrentPage( 1 );
    m_state = "play_story";
    m_stateStack.push( m_state );
    Logger::Debug( "Push " + m_state, "GOTO" );
}

void TitleState::Goto_Play_LevelSelect( int world )
{
    SetupMenu_LevelSelectScreen();
    menuManager.SetCurrentPage( 1 );
    m_state = "play_levelselect";
    m_stateStack.push( m_state );
    Logger::Debug( "Push " + m_state, "GOTO" );
}

void TitleState::Goto_Options()
{
    SetupMenu_OptionsScreen();
    menuManager.SetCurrentPage( 1 );
    m_state = "options";
    m_stateStack.push( m_state );
    Logger::Debug( "Push " + m_state, "GOTO" );
}

void TitleState::Goto_Help()
{
    SetupMenu_HelpScreen();
    menuManager.SetCurrentPage( 2 );
    m_state = "help";
    m_stateStack.push( m_state );
    Logger::Debug( "Push " + m_state, "GOTO" );
}

void TitleState::Goto_Help_HowToPlay()
{
    SetupMenu_HelpScreen();
    menuManager.SetCurrentPage( 3 );
    m_state = "help_howtoplay";
    m_stateStack.push( m_state );
    Logger::Debug( "Push " + m_state, "GOTO" );
}

void TitleState::Goto_Help_Credits()
{
    menuManager.SetCurrentPage( 4 );
    m_state = "help_credits";
    m_stateStack.push( m_state );
    Logger::Debug( "Push " + m_state, "GOTO" );
}

void TitleState::Draw()
{
    menuManager.Draw();
}

void TitleState::AdjustVolume( const std::string& buttonName )
{
    int music = StringUtil::StringToInt( kuko::ConfigManager::GetOption( "music_volume" ) );
    int sound = StringUtil::StringToInt( kuko::ConfigManager::GetOption( "sound_volume" ) );

    if ( buttonName == "btn_minus_music" )
    {
        music -= 33;
    }
    else if ( buttonName == "btn_plus_music" )
    {
        music += 33;
    }

    if ( buttonName == "btn_minus_sound" )
    {
        sound -= 33;
    }
    else if ( buttonName == "btn_plus_sound" )
    {
        sound += 33;
    }

    if ( music > 100 ) { music = 100; }
    else if ( music < 0 ) { music = 0; }
    if ( sound > 100 ) { sound = 100; }
    else if ( sound < 0 ) { sound = 0; }

    // Update config
    kuko::ConfigManager::SetOption( "music_volume", StringUtil::IntToString( music ) );
    kuko::ConfigManager::SetOption( "sound_volume", StringUtil::IntToString( sound ) );
    kuko::ConfigManager::SaveConfig();

    kuko::SoundManager::SetMusicVolume( music );
    kuko::SoundManager::SetSoundVolume( sound );

    // Update graphics
    music /= 33;
    sound /= 33;

    menuManager.GetButton( "sound_slider" )->SetFrame( kuko::IntRect( 0, sound * 40, 294, 40 ) );
    menuManager.GetButton( "music_slider" )->SetFrame( kuko::IntRect( 0, music * 40, 294, 40 ) );

    kuko::SoundManager::PlaySound( "button_confirm" );
}

void TitleState::SetupMenu_TitleScreen()
{
    Logger::Debug( "Menu Transition", "TitleState::SetupMenu_TitleScreen" );
    menuManager.LoadMenuScript( m_contentPath + "menus/title_screen.lua" );
    for ( int i = 0; i < menuManager.AssetListSize(); i++ )
    {
        kuko::ImageManager::AddTexture( menuManager.AssetTitle( i ), menuManager.AssetPath( i ) );
    }
    menuManager.BuildMenu();

    menuManager.AddLabel( "game_creator",
        kuko::LanguageManager::Text( "game_year" ) + " " + kuko::LanguageManager::Text( "game_creator" ) + ", "
        + " " + kuko::LanguageManager::Text( "game_version" ) + "     " + kuko::LanguageManager::Text( "game_website" ),
        830, 690, 0, 25, false, { 0xFF, 0xFF, 0xFF, 0xFF }, kuko::FontManager::GetFont( "main" ) );
}

void TitleState::SetupMenu_OptionsScreen()
{
    Logger::Debug( "Menu Transition", "TitleState::SetupMenu_OptionsScreen" );
    menuManager.LoadMenuScript( m_contentPath + "menus/help_options.lua" );
    for ( int i = 0; i < menuManager.AssetListSize(); i++ )
    {
        kuko::ImageManager::AddTexture( menuManager.AssetTitle( i ), menuManager.AssetPath( i ) );
    }
    menuManager.BuildMenu();

    // Set the sliders to match the config volume
    AdjustVolume( "" );
}

void TitleState::SetupMenu_HelpScreen()
{
    Logger::Debug( "Menu Transition", "TitleState::SetupMenu_HelpScreen" );
    menuManager.LoadMenuScript( m_contentPath + "menus/help_options.lua" );
    for ( int i = 0; i < menuManager.AssetListSize(); i++ )
    {
        kuko::ImageManager::AddTexture( menuManager.AssetTitle( i ), menuManager.AssetPath( i ) );
    }
    menuManager.BuildMenu();
}

void TitleState::SetupMenu_StoryScreen()
{
    Logger::Debug( "Menu Transition", "TitleState::SetupMenu_StoryScreen" );
    menuManager.LoadMenuScript( m_contentPath + "menus/storymode.lua" );
    for ( int i = 0; i < menuManager.AssetListSize(); i++ )
    {
        kuko::ImageManager::AddTexture( menuManager.AssetTitle( i ), menuManager.AssetPath( i ) );
    }
    menuManager.BuildMenu();
}

void TitleState::SetupMenu_LevelSelectScreen()
{
    Logger::Debug( "Menu Transition", "TitleState::SetupMenu_LevelSelectScreen" );
    menuManager.LoadMenuScript( m_contentPath + "menus/level_select.lua" );
    for ( int i = 0; i < menuManager.AssetListSize(); i++ )
    {
        kuko::ImageManager::AddTexture( menuManager.AssetTitle( i ), menuManager.AssetPath( i ) );
    }
    menuManager.BuildMenu();
}
