#include "SplashScreenState.hpp"

#include "../Kuko/managers/ImageManager.hpp"
#include "../Kuko/managers/FontManager.hpp"
#include "../Kuko/managers/InputManager.hpp"
#include "../Kuko/base/Application.hpp"
#include "../Kuko/utilities/StringUtil.hpp"
#include "../Kuko/utilities/Logger.hpp"

SplashScreenState::SplashScreenState( const std::string& contentPath )
{
}

SplashScreenState::~SplashScreenState()
{
}

void SplashScreenState::Setup()
{
    m_countdownMax = 0.6;
    m_totalElapsedTime = 0;
    m_countdown = m_countdownMax;
    kuko::ImageManager::AddTexture( "background",   "content/graphics/ui/splash/background.png" );
    kuko::ImageManager::AddTexture( "logo",         "content/graphics/ui/splash/logo.png" );

    m_background.SetTexture( kuko::ImageManager::GetTexture( "background" ) );
    m_logo.SetTexture( kuko::ImageManager::GetTexture( "logo" ) );

    int w = kuko::Application::GetDefaultWidth();
    int h = kuko::Application::GetDefaultHeight();
    m_logo.SetPosition( kuko::FloatRect( w/2 - 1055/2, h/2 - 442/2, 1055, 442 ) );
    m_logo.SetAlpha( 255 );

    m_disclaimer.SetFont( kuko::FontManager::GetFont( "main" ) );
    m_disclaimer.SetColor( 255, 255, 255, 255 );
    m_disclaimer.SetCentered( true );
    m_disclaimer.SetPosition( 0, 10, 60, 20 );
    m_disclaimer.ChangeText( "This build represents a work in progress. Bugs will be fixed, art will be changed, and more." );
}

void SplashScreenState::Cleanup()
{
}

void SplashScreenState::Update()
{
    kuko::InputManager::Update();
    std::map<kuko::CommandButton, kuko::TriggerInfo> input = kuko::InputManager::GetTriggerInfo();
    std::string keyHit = kuko::InputManager::GetKeyHit();
    bool leftTap = input[ kuko::TAP ].down;

    if ( keyHit != "" || leftTap )
    {
        m_gotoState = "title";
    }

    m_totalElapsedTime += kuko::Application::GetTimeStep();
    if ( m_totalElapsedTime >= m_countdownMax )
    {
        m_gotoState = "title";
    }

//    Uint16 alpha = m_logo.GetAlpha();
//
//    if ( m_countdown >= m_countdownMax*2/3 && m_countdown <= m_countdownMax )
//    {
//        // Fade In
//        alpha = m_countdownMax - m_countdown * 300 / m_countdownMax;
//        if ( alpha > 255 ) { alpha = 255; }
//        Logger::Debug( "FADE IN - Countdown: " + StringUtil::FloatToString( m_countdown ) + " - Alpha: " + StringUtil::FloatToString( alpha ), "SplashScreenState::Update" );
//    }
//    else if ( m_countdown > m_countdownMax*1/3 && m_countdown < m_countdownMax*2/3 )
//    {
//        // Pause
//        Logger::Debug( "PAUSE - Countdown: " + StringUtil::FloatToString( m_countdown ) + " - Alpha: " + StringUtil::FloatToString( alpha ), "SplashScreenState::Update" );
//    }
//    else if ( m_countdown >= 0 && m_countdown <= m_countdownMax*1/3 )
//    {
//        // Fade Out
//        alpha = m_countdown * 300 / m_countdownMax;
//        if ( alpha < 0 ) { alpha = 0; }
//        Logger::Debug( "FADE OUT - Countdown: " + StringUtil::FloatToString( m_countdown ) + " - Alpha: " + StringUtil::FloatToString( alpha ), "SplashScreenState::Update" );
//    }
//
//    m_logo.SetAlpha( alpha );
}

void SplashScreenState::Draw()
{
    m_background.Draw();
    m_logo.Draw();
    m_disclaimer.Draw();
}

void SplashScreenState::SetContentPath( const std::string& path )
{
    m_contentPath = path;
}

