#include "CutsceneState.hpp"

#include "../Kuko/managers/ImageManager.hpp"
#include "../Kuko/managers/InputManager.hpp"
#include "../Kuko/managers/FontManager.hpp"
#include "../Kuko/managers/LanguageManager.hpp"
#include "../Kuko/managers/SoundManager.hpp"
#include "../Kuko/managers/ConfigManager.hpp"
#include "../Kuko/managers/StateManager.hpp"
#include "../Kuko/utilities/Logger.hpp"
#include "../Kuko/base/Application.hpp"
#include "../Kuko/utilities/StringUtil.hpp"

CutsceneState::~CutsceneState()
{
}

CutsceneState::CutsceneState( const std::string& contentPath ) : IState()
{
    SetContentPath( contentPath );
}

void CutsceneState::Setup()
{
    Logger::Out( "Setup", "CutsceneState" );
    kuko::IState::Setup();
    SetupMenu();
}

void CutsceneState::Cleanup()
{
    Logger::Out( "Cleanup", "CutsceneState::Cleanup" );
    kuko::ImageManager::ClearTextures();
}

void CutsceneState::SetContentPath( const std::string& path )
{
    m_contentPath = path;
}

void CutsceneState::Update()
{
    kuko::InputManager::Update();
    std::map<kuko::CommandButton, kuko::TriggerInfo> input = kuko::InputManager::GetTriggerInfo();

    if ( input[ kuko::WINDOW_CLOSE ].down )
    {
        m_gotoState = "quit";
    }
    else if ( input[ kuko::TAP ].down )
    {
        int x = input[ kuko::TAP ].actionX, y = input[ kuko::TAP ].actionY;
        std::string clickedButtonId = menuManager.GetButtonClicked( x, y );

        if ( clickedButtonId == "btn_cutscene_next" )
        {
            if ( menuManager.GetCurrentPage() == menuManager.GetTotalPages() )
            {
                // Get previous state
                kuko::StateManager::PopState();
                m_gotoState = kuko::StateManager::TopState();
                Logger::Out( "Pop to previous state: " + m_gotoState, "CutsceneState::Update" );
            }
            else
            {
                menuManager.NextPage();
            }
        }
    }

    menuManager.Update();
}

void CutsceneState::Draw()
{
    menuManager.Draw();
}

void CutsceneState::SetupMenu()
{
    m_state = "cutscene";
    menuManager.LoadMenuScript( m_contentPath + "menus/cutscene.lua" );
    for ( int i = 0; i < menuManager.AssetListSize(); i++ )
    {
        kuko::ImageManager::AddTexture( menuManager.AssetTitle( i ), menuManager.AssetPath( i ) );
    }
    menuManager.BuildMenu();

    int screenWidth = kuko::Application::GetDefaultWidth();
}

