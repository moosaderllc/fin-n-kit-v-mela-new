#ifndef JUGGLEGAMESTATE_HPP
#define JUGGLEGAMESTATE_HPP

#include "../Kuko/states/IState.hpp"
#include "../Kuko/widgets/UIButton.hpp"

#include "../worlds/Level.hpp"
#include "../entities/JumpingObject.hpp"

enum SubState { PLAYING, PAUSED, LEVEL_FAIL, LEVEL_SUCCESS };

class JuggleGameState : public kuko::IState
{
    public:
    JuggleGameState( const std::string& contentPath );

    virtual void Setup();
    virtual void Cleanup();

    virtual void Update();
    virtual void Draw();

    private:
    void UpdatePlaying();
    void UpdateMenu();

    void SetupPauseMenu();
    void SetupSuccessMenu();
    void SetupFailureMenu();
    void SetupLevel();
    void SetupCharacters();
    void ScrollLevel();
    void HandleCollisions();

    void FinJump();
    void KitJump();

    std::string m_contentPath;
    float m_scrollSpeed;
    int m_effectCounter;
    std::string m_levelPath;
    Level m_level;

    JumpingObject m_fin;
    JumpingObject m_kit;
    kuko::Sprite m_extraLife;
    bool m_hasExtraLife;
    bool m_hasHitObstacle;
    bool m_collectedAllTrinkets;

    kuko::UIButton m_pauseButton;
    SubState m_subState;
    kuko::Sprite m_shadebg;
    kuko::Sprite m_pausebg;
    kuko::UIButton m_restartButton;
    kuko::UIButton m_exitButton;
    kuko::UIButton m_continueButton;
    kuko::UILabel m_menuHeader;
    kuko::UILabel m_buttonLabel1;
    kuko::UILabel m_buttonLabel2;
    kuko::UILabel m_buttonLabel3;

    kuko::Sprite m_scores[3];
};

#endif
