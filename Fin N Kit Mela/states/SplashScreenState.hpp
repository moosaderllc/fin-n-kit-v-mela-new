#ifndef SPLASH_SCREEN_HPP
#define SPLASH_SCREEN_HPP

#include "../Kuko/states/IState.hpp"
#include "../Kuko/managers/MenuManager.hpp"

#include <vector>
#include <string>

class SplashScreenState : public kuko::IState
{
    public:
    SplashScreenState( const std::string& contentPath );
    virtual ~SplashScreenState();

    virtual void Setup();
    virtual void Cleanup();
    virtual void Update();
    virtual void Draw();

    virtual void SetContentPath( const std::string& path );

    private:
    float m_countdown;
    float m_countdownMax;
    float m_totalElapsedTime;

    kuko::Sprite m_background;
    kuko::Sprite m_logo;
    kuko::UILabel m_disclaimer;

    std::string m_state;

    std::string m_contentPath;
};

#endif
