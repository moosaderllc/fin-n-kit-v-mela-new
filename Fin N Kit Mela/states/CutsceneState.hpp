#ifndef CUTSCENE_STATE_HPP
#define CUTSCENE_STATE_HPP

#include "../Kuko/states/IState.hpp"
#include "../Kuko/managers/MenuManager.hpp"

#include <vector>
#include <string>

class CutsceneState : public kuko::IState
{
    public:
    CutsceneState( const std::string& contentPath );
    virtual ~CutsceneState();

    virtual void Setup();
    virtual void Cleanup();
    virtual void Update();
    virtual void Draw();

    virtual void SetContentPath( const std::string& path );

    private:
    kuko::MenuManager menuManager;

    void SetupMenu();

    std::string m_state;

    std::string m_contentPath;
};

#endif
