#include "LanguageSelectState.hpp"

#include "../Kuko/managers/ImageManager.hpp"
#include "../Kuko/managers/InputManager.hpp"
#include "../Kuko/managers/FontManager.hpp"
#include "../Kuko/managers/LanguageManager.hpp"
#include "../Kuko/managers/SoundManager.hpp"
#include "../Kuko/managers/ConfigManager.hpp"
#include "../Kuko/utilities/Logger.hpp"
#include "../Kuko/base/Application.hpp"
#include "../Kuko/utilities/StringUtil.hpp"

LanguageSelectState::~LanguageSelectState()
{
}

LanguageSelectState::LanguageSelectState( const std::string& contentPath ) : IState()
{
    SetContentPath( contentPath );
}

void LanguageSelectState::Setup()
{
    Logger::Out( "Setup", "LanguageSelectState" );
    kuko::IState::Setup();
    SetupMenu();
}

void LanguageSelectState::Cleanup()
{
    Logger::Out( "Cleanup", "LanguageSelectState::Cleanup" );
    kuko::ImageManager::ClearTextures();
}

void LanguageSelectState::SetContentPath( const std::string& path )
{
    m_contentPath = path;
}

void LanguageSelectState::Update()
{
    kuko::InputManager::Update();
    std::map<kuko::CommandButton, kuko::TriggerInfo> input = kuko::InputManager::GetTriggerInfo();

    if ( input[ kuko::WINDOW_CLOSE ].down )
    {
        m_gotoState = "quit";
    }
    else if ( input[ kuko::TAP ].down )
    {
        int x = input[ kuko::TAP ].actionX, y = input[ kuko::TAP ].actionY;

        bool languageChosen = false;

        if( menuManager.IsButtonClicked( "btn_english", x, y ) )
        {
            kuko::ConfigManager::SetOption( "language", "english" );
            languageChosen = true;
        }

        else if ( menuManager.IsButtonClicked( "btn_spanish", x, y ) )
        {
            kuko::ConfigManager::SetOption( "language", "spanish" );
            languageChosen = true;
        }

        else if ( menuManager.IsButtonClicked( "btn_esperanto", x, y ) )
        {
            kuko::ConfigManager::SetOption( "language", "esperanto" );
            languageChosen = true;
        }

        if ( languageChosen )
        {
            kuko::ConfigManager::SaveConfig();
            kuko::LanguageManager::AddLanguage( "primary", m_contentPath + "languages/" + kuko::ConfigManager::GetOption( "language" ) + ".lua" );
            m_gotoState = "title";
        }
    }

    menuManager.Update();
}

void LanguageSelectState::Draw()
{
    menuManager.Draw();
}

void LanguageSelectState::SetupMenu()
{
    m_state = "language_select_state";
    menuManager.LoadMenuScript( m_contentPath + "menus/language_select.lua" );
    for ( int i = 0; i < menuManager.AssetListSize(); i++ )
    {
        kuko::ImageManager::AddTexture( menuManager.AssetTitle( i ), menuManager.AssetPath( i ) );
    }
    menuManager.BuildMenu();

    int screenWidth = kuko::Application::GetDefaultWidth();
}

