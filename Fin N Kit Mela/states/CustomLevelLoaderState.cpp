#include "CustomLevelLoaderState.hpp"

#include "../Kuko/managers/ImageManager.hpp"
#include "../Kuko/managers/InputManager.hpp"
#include "../Kuko/managers/FontManager.hpp"
#include "../Kuko/managers/LanguageManager.hpp"
#include "../Kuko/managers/SoundManager.hpp"
#include "../Kuko/managers/ConfigManager.hpp"
#include "../Kuko/utilities/Logger.hpp"
#include "../Kuko/base/Application.hpp"
#include "../Kuko/utilities/StringUtil.hpp"
#include "../utils/Messager.hpp"

CustomLevelLoaderState::~CustomLevelLoaderState()
{
}

CustomLevelLoaderState::CustomLevelLoaderState( const std::string& contentPath ) : IState()
{
    Logger::Out( "Content path: " + contentPath, "CustomLevelLoaderState" );
    SetContentPath( contentPath );
}

void CustomLevelLoaderState::Setup()
{
    Logger::Out( "Setup", "CustomLevelLoaderState" );
    kuko::IState::Setup();
    SetupMenu();
    SetupLevelButtons();
}

void CustomLevelLoaderState::Cleanup()
{
    kuko::ImageManager::ClearTextures();
    m_lstSaveGames.clear();
}

void CustomLevelLoaderState::SetContentPath( const std::string& path )
{
    m_contentPath = path;
}

void CustomLevelLoaderState::Update()
{
    kuko::InputManager::Update();
    std::map<kuko::CommandButton, kuko::TriggerInfo> input = kuko::InputManager::GetTriggerInfo();

    if ( input[ kuko::WINDOW_CLOSE ].down )
    {
        m_gotoState = "quit";
    }
    else if ( input[ kuko::TAP ].down )
    {
        int x = input[ kuko::TAP ].actionX, y = input[ kuko::TAP ].actionY;
        std::string clickedButtonId = menuManager.GetButtonClicked( x, y );

        if ( clickedButtonId.find( "edit" ) != std::string::npos )
        {
            Messager::msg["level"] = clickedButtonId.substr( 5, clickedButtonId.size() );
            m_gotoState = "level_editor";
        }

        else if ( clickedButtonId.find( ".fin" ) != std::string::npos )
        {
            Messager::msg["previousState"] = "custom_level_select";
            Messager::msg["level"] = clickedButtonId;
            kuko::SoundManager::PlaySound( "button_confirm" );
            m_gotoState = "juggle_game";
        }

        else if ( clickedButtonId == "btn_back" )
        {
            kuko::SoundManager::PlaySound( "button_cancel" );
            m_gotoState = "title";
        }
    }

    menuManager.Update();
}

void CustomLevelLoaderState::SetupLevelButtons()
{
    std::ifstream input( "maps.txt" );

    int buttonHeight = 80;
    int count = 0;
    int x = 50;
    int y = (buttonHeight+20);

    std::string level;
    while ( getline( input, level ) )
    {
        kuko::FloatRect txtPos = { x + 60, y, 464, 80 };
        kuko::FloatRect editPos = { x, y, 50, 50 };
        kuko::FloatRect lblPos = { x + 80 , y+10, 464, 60 };
        kuko::UIButton* button = new kuko::UIButton;
        kuko::UIButton* editButton = new kuko::UIButton;
        kuko::UILabel* label = new kuko::UILabel;

        button->Setup( level, txtPos, false, kuko::ImageManager::GetTexture( "button_main" ), { 0xFF, 0xFF, 0xFF, 0xFF } );
        editButton->Setup( "edit_" + level, editPos, false, kuko::ImageManager::GetTexture( "button_edit" ), { 0xFF, 0xFF, 0xFF, 0xFF } );
        label->Setup( level, level, lblPos, false, { 0xFF, 0xFF, 0xFF, 0xFF }, kuko::FontManager::GetFont( "main" ) );

        menuManager.AddButton( level, button );
        menuManager.AddButton( "edit_" + level, editButton );
        menuManager.AddLabel( level, label );

        count++;
        y += 100;
        if ( count == 5 )
        {
            y = (buttonHeight+20);
            x += 500;
            count = 0;
        }
    }
    input.close();
}

void CustomLevelLoaderState::Draw()
{
    menuManager.Draw();
}

void CustomLevelLoaderState::SetupMenu()
{
    m_state = "custom_loader";
    menuManager.LoadMenuScript( m_contentPath + "menus/custom_loader.lua" );
    for ( int i = 0; i < menuManager.AssetListSize(); i++ )
    {
        kuko::ImageManager::AddTexture( menuManager.AssetTitle( i ), menuManager.AssetPath( i ) );
    }
    menuManager.BuildMenu();

    int screenWidth = kuko::Application::GetDefaultWidth();
}
