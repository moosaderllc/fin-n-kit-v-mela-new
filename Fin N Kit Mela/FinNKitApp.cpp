#include "FinNKitApp.hpp"

#include "states/TitleState.hpp"
#include "states/LanguageSelectState.hpp"
#include "states/LevelEditorState.hpp"
#include "states/JuggleGameState.hpp"
#include "states/CustomLevelLoaderState.hpp"
#include "states/SplashScreenState.hpp"
#include "states/CutsceneState.hpp"

#include "Kuko/utilities/Platform.hpp"

#include <iostream>

std::string FinNKitApp::m_gotoLevel = "";

FinNKitApp::FinNKitApp()
{
    Logger::Setup();
    Logger::Out( "Application Begin!" );
    m_contentPath = "content/";
    m_goToLanguageSelect = false;
    Setup();
}

FinNKitApp::~FinNKitApp()
{
    Logger::Out( "Application Destroy!" );
    Cleanup();
}

void FinNKitApp::Run()
{
    if ( m_goToLanguageSelect )
    {
        // Go to language select first
        kuko::LanguageManager::AddLanguage( "common", m_contentPath + "languages/common.lua" );
        m_stateManager.PushState( "language_select" );
    }
    else
    {
        // Go to title state first
        kuko::LanguageManager::AddLanguage( "primary", m_contentPath + "languages/" + kuko::ConfigManager::GetOption( "language" ) + ".lua" );
        m_stateManager.PushState( "splash" );
        //m_stateManager.SwitchState( "cutscene" );
    }

    Logger::Out( "Begin main game loop", "FinNKitApp::Run()" );
    kuko::Application::ResetStepTimer();
    while ( !m_stateManager.IsDone() )
    {
        // Timer
        kuko::Application::TimerStart();

        // Update
        m_stateManager.UpdateCurrentState();

        kuko::Application::ResetStepTimer();

        // Draw
        kuko::Application::BeginDraw();
        m_stateManager.DrawCurrentState();
        kuko::Application::EndDraw();

        // Timer
        kuko::Application::TimerUpdate();
    }
}

void FinNKitApp::Cleanup()
{
    Logger::Out( "Clean up the game!", "Cleanup" );
    kuko::ConfigManager::Cleanup();
    kuko::Application::End();
    Logger::Cleanup();
}

void FinNKitApp::Setup()
{
    KukoSetup();
    AppSetup();
}

void FinNKitApp::KukoSetup()
{
    Logger::Out( "Set up Fin 'n' Kit!", "FinNKitApp::Setup()" );

    // Lua Manager
    kuko::LuaManager::Setup();

    // Config Manager
    kuko::ConfigManager::Setup();
    std::vector<std::string> configSettings;
    configSettings.push_back( "language" );
    configSettings.push_back( "screen_width" );
    configSettings.push_back( "screen_height" );
    configSettings.push_back( "music_volume" );
    configSettings.push_back( "sound_volume" );
    configSettings.push_back( "vsync" );
    configSettings.push_back( "fullscreen" );
    configSettings.push_back( "borderless" );

    OperatingSystem platform = Platform::GetOperatingSystem();
    Logger::Out( "I think that the operating system build is: " + Platform::GetOperatingSystemName( platform ), "FinNKitApp::KukoSetup" );

    bool firstRun = ( kuko::ConfigManager::LoadConfig( configSettings ) == false );
    if ( firstRun )
    {
        Logger::Out( "This is a first run - Set up the initial config file.", "Setup" );
        kuko::ConfigManager::SetOption( "language", "english" );
        kuko::ConfigManager::SetOption( "screen_width", "1280" );
        kuko::ConfigManager::SetOption( "screen_height", "720" );
        kuko::ConfigManager::SetOption( "music_volume", "100" );
        kuko::ConfigManager::SetOption( "sound_volume", "100" );
        kuko::ConfigManager::SetOption( "fullscreen", "false" );
        kuko::ConfigManager::SetOption( "borderless", "false" );

        if ( platform == WINDOWS32 || platform == WINDOWS64 )
        {
            kuko::ConfigManager::SetOption( "vsync", "false" );
        }
        else
        {
            kuko::ConfigManager::SetOption( "vsync", "true" );
        }
        kuko::ConfigManager::SaveConfig();
        // Need to go to language select if this is the first run.
        m_goToLanguageSelect = true;
    }

    // Language Manager
    kuko::LanguageManager::Setup();
    kuko::LanguageManager::AddLanguage( "common", m_contentPath + "languages/common.lua" );

    bool useVsync = ( kuko::ConfigManager::GetOption( "vsync" ) == "true" );
    bool fullscreen = false;
        //( kuko::ConfigManager::GetOption( "fullscreen" ) == "true" );
    bool borderless = true;
        //( kuko::ConfigManager::GetOption( "borderless" ) == "true" );
    bool fitToScreen = false;
    // Application
    kuko::Application::Start( kuko::LanguageManager::Text( "game_title" ) + " - " + kuko::LanguageManager::Text( "game_creator" ),
        StringUtil::StringToInt( kuko::ConfigManager::GetOption( "screen_width" ) ),
        StringUtil::StringToInt( kuko::ConfigManager::GetOption( "screen_height" ) ),
        1280, 720, useVsync, fullscreen, borderless, fitToScreen );

    // Font Manager
    kuko::FontManager::Setup();
    kuko::FontManager::AddFont( "main", m_contentPath + "fonts/NotoSans-Bold.ttf", 60 );

    // Input Manager
    kuko::InputManager::Setup();

    // Sound Manager
    kuko::SoundManager::AddMusic( "title", m_contentPath + "audio/UpbeatForever_Incompetech.mp3" );
    kuko::SoundManager::AddMusic( "cutscene", m_contentPath + "audio/Harlequin_Incompetech.mp3" );
    kuko::SoundManager::AddMusic( "level1", m_contentPath + "audio/CarnivaleIntrigue_Incompetech.mp3" );
    kuko::SoundManager::AddMusic( "level2", m_contentPath + "audio/Motherlode_Incompetech.mp3" );
    kuko::SoundManager::AddMusic( "level3", m_contentPath + "audio/ThiefInTheNight_Incompetech.mp3" );

    kuko::SoundManager::AddSound( "button_confirm", m_contentPath + "audio/confirm.wav" );
    kuko::SoundManager::AddSound( "button_cancel", m_contentPath + "audio/cancel.wav" );
    kuko::SoundManager::AddSound( "collect_dead", m_contentPath + "audio/collect_dead.wav" );
    kuko::SoundManager::AddSound( "collect_bad", m_contentPath + "audio/collect_bad.wav" );
    kuko::SoundManager::AddSound( "collect_good", m_contentPath + "audio/collect_good.wav" );
    kuko::SoundManager::AddSound( "jump_fin", m_contentPath + "audio/jump_fin.wav" );
    kuko::SoundManager::AddSound( "jump_kit", m_contentPath + "audio/jump_kit.wav" );

    kuko::SoundManager::SetMusicVolume( StringUtil::StringToInt( kuko::ConfigManager::GetOption( "music_volume" ) ) );
    kuko::SoundManager::SetSoundVolume( StringUtil::StringToInt( kuko::ConfigManager::GetOption( "sound_volume" ) ) );
}

void FinNKitApp::AppSetup()
{
    // Add states
    m_stateManager.AddState( "splash",             new SplashScreenState( m_contentPath ) );
    m_stateManager.AddState( "title",              new TitleState( m_contentPath ) );
    m_stateManager.AddState( "language_select",    new LanguageSelectState( m_contentPath ) );
    m_stateManager.AddState( "level_editor",       new LevelEditorState( m_contentPath ) );
    m_stateManager.AddState( "juggle_game",        new JuggleGameState( m_contentPath ) );
    m_stateManager.AddState( "custom_level_select",new CustomLevelLoaderState( m_contentPath ) );
    m_stateManager.AddState( "cutscene",           new CutsceneState( m_contentPath ) );
}

std::string FinNKitApp::GetGotoLevel()
{
    return m_gotoLevel;
}

void FinNKitApp::SetGotoLevel( const std::string& level )
{
    m_gotoLevel = level;
}







