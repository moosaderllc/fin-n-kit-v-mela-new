#ifndef MESSAGER_HPP
#define MESSAGER_HPP

#include <string>
#include <map>

class Messager
{
    public:
    static std::map<std::string, std::string> msg;
};

#endif
