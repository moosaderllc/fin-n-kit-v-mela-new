Readme

Fin 'n' Kit (v. Mela)
Dev Version, build 2016.05.03 (Win x86)
Moosader LLC

Working areas: Level Editor and Custom Level loader.
You can make and test levels!

Levels get stored in the "levels" folder.

If there are any errors, please send me the "log.html" file!

Rachel@Moosader.com