#ifndef FINNKITAPP_HPP
#define FINNKITAPP_HPP

#include "Kuko/base/Application.hpp"
#include "Kuko/utilities/Logger.hpp"
#include "Kuko/utilities/StringUtil.hpp"
#include "Kuko/managers/ImageManager.hpp"
#include "Kuko/managers/StateManager.hpp"
#include "Kuko/managers/InputManager.hpp"
#include "Kuko/managers/FontManager.hpp"
#include "Kuko/managers/LanguageManager.hpp"
#include "Kuko/managers/LuaManager.hpp"
#include "Kuko/managers/SoundManager.hpp"
#include "Kuko/managers/ConfigManager.hpp"

#include <string>

class FinNKitApp
{
    public:
    FinNKitApp();
    ~FinNKitApp();

    void Run();
    void Cleanup();

    private:
    void Setup();
    void KukoSetup();
    void AppSetup();

    std::string m_contentPath;
    bool m_goToLanguageSelect;

    kuko::StateManager m_stateManager;

    static std::string GetGotoLevel();
    static void SetGotoLevel( const std::string& level );

    private:
    static std::string m_gotoLevel;
};

#endif
