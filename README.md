![Level editor screenshot](https://bytebucket.org/moosaderllc/fin-n-kit-v-mela-new/raw/07be477384779c9ea7d07d65298d9566ed823872/blog/2016-05-10/Screenshot-Fin%20%27n%27%20Kit%20-%20Moosader%20LLC.png?token=826ec310d0812c24dcd3eeec39b83100b1ea22e5)

See the wiki for info on cloning and setup: [http://wiki.moosader.com/doku.php?id=start](http://wiki.moosader.com/doku.php?id=start)

## [Tutorial Video](https://www.youtube.com/watch?v=FhJPanQL1HI)

# Fin 'n' Kit

Mela (Ocean) Version

## Purpose

This is the latest version of Fin 'n' Kit built as a port of the original, which was made with Gideros and Lua.

This version uses C++ and Lua with Rachel's Kuko framework. This decision was made due to some constraints with Gideros:

* No support for most keyboard keys, except letters and arrow keys.
* No support for choosing a hard-drive location to save data to.
* No support for Linux systems.

However there will be some challenges working with Kuko to make it available for mobile:

* How to embed ads?
* How to build Lua for Android?
* Making sure screen resizes on Android devices
* Selecting hard-drive path for sdcard on Android devices

Once these are resolved, it will be good for all future Moosader projects.

## Build Information

**Linker Settings**

/usr/lib/x86_64-linux-gnu/libSDL2.a;
/usr/lib/x86_64-linux-gnu/libSDL2_image.a;
/usr/lib/x86_64-linux-gnu/libSDL2main.a;
/usr/lib/x86_64-linux-gnu/libSDL2_mixer.a;
/usr/lib/x86_64-linux-gnu/libSDL2_ttf.a;
/usr/lib/x86_64-linux-gnu/liblua5.2.a;

**Other linker options:**

-lSDL2

-lSDL2_image

-lSDL2_ttf

-lSDL2_mixer

-llua5.2

**Search directories - Compiler**

/usr/include/SDL2

/usr/include/lua5.2

**Search directories - Linker**

/usr/lib/x86_64-linux-gnu/
